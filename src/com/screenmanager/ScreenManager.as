﻿/**
 * ScreenManager.as
 *
 * The Screenmanager helps to create game screens for eac
 * http://nielsvandeursen.com/?p=49
 */

package com.screenmanager
{
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	
	public class ScreenManager extends MovieClip
	{
		private var container:DisplayObjectContainer;		// Will contain all our screen display objects.
		private var currentScreen:Screen;					// Will represent our currently active screen that has controlls.
		private var screens:Array							// Will hold all the screens that are currently active.
		private var index:Number;							// Will hold the index in which our childs are added.
		
		
		// Constructor
		public function ScreenManager(container:DisplayObjectContainer, index:Number)
		{
			this.container = container;
			this.index = index;
			screens = new Array();
		}
		
		
		public function LoadScreen(screen:Screen):void
		{
			// Temporary variable.
			var updateScreen:Screen = screen;
			
			if (activeScreenExists())
			{
				if (!updateScreen.IsPopup)
				{
					// If our new screen is not a popup it means it is a full new screen.
					// Therefor the screen that is currently active should exit.
					UnloadAllScreens();
					
					// Set the screen properties.
					SetupUpdateScreen(updateScreen);
					
					// And put it in our array.
					screens.push(updateScreen);
				}
				else
				{
					// If our new screen is a popup, we only need to disable the controlls on other screens.
					for each (var sc:Screen in screens)
					{
						trace(sc + sc.HasControls);
						sc.HasControls = false;
					}
					
					SetupUpdateScreen(updateScreen);
					screens.push(updateScreen);
				}
			}
			else
			{
				// If there aren't any active screens we can just activate the new one.
				SetupUpdateScreen(updateScreen);
				
				screens.push(updateScreen);
			}
			updateScreen.HasControls = true;
		}
		
		
		/**
		 * Set's the properties of a new screen and executes it.
		 * @param	updateScreen
		 */
		private function SetupUpdateScreen(updateScreen:Screen):void
		{
			updateScreen.ScreenManager = this;
			updateScreen.HasControls = true;
			container.addChild(updateScreen);
			updateScreen.Enter();
		}
		
		
		
		/**
		 * Removes the screens that have a completely transitioned out.
		 */
		public function RemoveScreen():void
		{
			for each (var s:Screen in screens)
			{
				// Just to be sure.
				s.HasControls = false;
				
				if (s.HasExited)
				{
					screens.splice(screens.indexOf(s, 0), 1);
					container.removeChild(s);
				}
			}
			
			// Make the topmost screen in the array the active screen.
			screens[screens.length - 1].HasControls = true;
		}
			
		
		public function UnloadAllScreens():void
		{
			for each (var s:Screen in screens)
			{
				s.Exit();
				trace(s + s.HasControls);
				s.HasControls = false;
			}
		}
		
		
		/**
		 * Check's if there's any screen in existence.
		 * @return
		 */
		private function activeScreenExists():Boolean
		{
			for each (var screen:Screen in screens)
			{
				return true;
			}
			return false;
		}
		
		/**
		 * Updates each screen in the screens array.
		 */
		public function Update():void
		{
			for each (var screen:Screen in screens)
			{
				screen.Update();
			}
		}
	}
}