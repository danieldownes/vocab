﻿package com.screenmanager
{
	import flash.display.MovieClip;
	import caurina.transitions.Tweener;
	import flash.display.Stage;
	
	public class Screen extends MovieClip
	{
		
		// These variables are pretty self explanatory.
		protected var screenManager:*;
		protected var isPopup:Boolean;
		protected var hasExited:Boolean;
		protected var transitionInTime:Number = 1;
		protected var transitionOutTime:Number = 1;
		protected var hasControls:Boolean;
		
		public function Screen()
		{
			alpha = 0;
		}
		
		
		/**
		 * Should be overrided and a transitionIn tween should be put here.
		 * If it is not overrided, a default transitionIn tween will be used.
		 */
		public function Enter():void
		{
			ScreenEnter();
		}
		
		public function ScreenEnter():void
		{
			Tweener.addTween(this, { x:0, y:0, alpha:1, time:transitionInTime,transition:"easeOut" } );
		}
		
		/**
		 * Should be overrided and a transitionOut tween should be put here.
		 * If it is not overrided, a default transitionOut tween will be used.
		 */
		public function Exit():void
		{
			ScreenExit();
		}
		
		public function ScreenExit():void
		{
			Tweener.addTween(this, { x:0, y:0, alpha:0, time:transitionOutTime, transition:"easeOut", onComplete:Remove } );
		}
		
		/**
		 * The user overrides this function and put's any clean up logic like removing event listeners in here.
		 */
		public function Cleanup():void
		{
			
		}
		
		
		/**
		 * Runs on EnterFrame, update logic goes in here.
		 */
		public function Update():void
		{
			
		}
		
		
		public static function CentreScreen(obj:Object):void
		{
			obj.x = Main.STAGE.stageWidth / 2 - obj.width / 2 ;
			obj.y = Main.STAGE.stageHeight / 2 - obj.height / 2;
		}
		
		
		/**
		 * Marks the screen for deletion so the screenmanager can delete it.
		 * It also runs the cleanup function.
		 */
		public function Remove():void
		{
			Cleanup();
			hasExited = true;
			screenManager.RemoveScreen();
		}
		
		
		// ScreenManager Property
		public function get ScreenManager():* {
			return screenManager;
		}
		public function set ScreenManager(value:*):void	{
			screenManager = value;
		}
		
		
		// IsPopup property
		public function get IsPopup():Boolean {
			return isPopup;
		}
		protected function set IsPopup(value:Boolean):void{
			isPopup = value;
		}
		
		
		// HasExited Property
		public function get HasExited():Boolean {
			return hasExited;
		}
		
		
		// HasControlls Property
		public function get HasControls():Boolean {
			return hasControls;
		}
		public function set HasControls(value:Boolean):void {
			hasControls = value;
		}
		
	}
}