package com.screenmanager
{
	
	public interface IScreen
	{
		function Enter():void;
		function Exit():void;
	}
	
}