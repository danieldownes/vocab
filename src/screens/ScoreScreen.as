﻿package screens
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.display.Loader;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import goapi.apis.GoApiAchievement;
	
	import goapi.GoApi;
	import goapi.apis.datatypes.GoApiScoreType;
	import goapi.apis.datatypes.GoApiAchievementType;
	import goapi.apis.events.ScoreResult;
	import game.LanguageManager;
	
	import screenpopups.AchievementPopup;
	import com.screenmanager.Screen;
	
	
	public class ScoreScreen extends Screen
	{
		private var _design:ScoreScreen_design = new ScoreScreen_design();
		
		private var bSharedScore:Boolean;
		private var score:int;
		private var rank:int;
		private var facebookName:String;
		
		private var _currLanguage:uint;
		
		public function ScoreScreen():void
		{
			super();
			addChild(_design);
			
			Main.singletron.setVolControlsVisible(true);
			
			// Select language
			_currLanguage = LanguageManager.getInstance().getLanguage();
			_design.mShareLang.gotoAndStop(1);
			
			score = Main.singletron.score;
			_design.txtScore.text = score.toString();
			
			_design.btnExit.addEventListener(MouseEvent.CLICK, btnExit_click );
			_design.btnShare.addEventListener(MouseEvent.CLICK, btnShare_click);
			_design.btnPlayAgain.addEventListener(MouseEvent.CLICK, btnPlayAgain_click );
			
			
			//TODO: Allow Facebook share score 
			_design.btnShare.alpha = 0.3;
			_design.btnShare.mouseEnabled = false;
			
			rank = GoApi.Instance.goScore.getRank(score);
			trace( "score screne score: " + score + " rank: " + rank);
			
			if( rank != -100)
			{
				_design.txtRank.text = LanguageManager.getInstance().getRankName(rank);
				_design.tInfo.text = LanguageManager.getInstance().getRankDesc(rank);
			}
			
			// Select language to show
			_design.gotoAndStop(_currLanguage + 1);
			
			// Load rank background
			loadRankScreen(rank.toString());
			
			// Not Guest play mode
			if( !GoApi.Instance.apiBase.guestPlay)
			{
				// Save score // TODO: include time elapsed
				GoApi.Instance.goScore.addEventListener(ScoreResult.SCORE_RESULT, goScore_result);
				var ScoreObj:GoApiScoreType = new GoApiScoreType();
				ScoreObj.score = score;
				ScoreObj.mode = 0;
				ScoreObj.data1 = "language:" + _currLanguage;
				ScoreObj.data2 = "wordsTot:" + Main.singletron.wordsTot + ",wordsFound:" + Main.singletron.wordsFound + ",rank" + rank;
				ScoreObj.userId = GoApi.Instance.apiBase.userId;
				ScoreObj.profileId = GoApi.Instance.apiBase.profileId;
				GoApi.Instance.goScore.save(ScoreObj);
				
				bSharedScore = false;
			}
		}
		
		private function goScore_result(e:ScoreResult):void 
		{
			trace("goScore_result = " + e._submitted);
		}
		
		private function btnExit_click(e:MouseEvent):void 
		{
			Main.singletron.screenManager.LoadScreen( new MenuScreen());
		}
		
		private function btnPlayAgain_click(e:MouseEvent):void 
		{
			Main.singletron.screenManager.LoadScreen( new InstructionsScreen());
		}
		
		
		private function loadRankScreen(sRank:String):void
		{
			var mLoader:Loader = new Loader();
			var mRequest:URLRequest = new URLRequest(Main.singletron.GAME_ASSETS + "rank_screens/rank" + sRank + ".swf");
			trace("sRank=" +  sRank);
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadRankScreen_complete);
			mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, loadRankScreen_progress);
			mLoader.load(mRequest);
			
			trace("loadRankScreen");
		}

		private function loadRankScreen_complete(loadEvent:Event):void
		{
			var newScreen:MovieClip = MovieClip(loadEvent.currentTarget.content);
			_design.mRankBg.addChild(newScreen);
			_design.mRankBg.x = 0;
			_design.mRankBg.y = 0;
			
			//setChildIndex(_design._profileScreen, numChildren -1 );
			
			trace("loadRankScreen_complete");
			
			// Hide preloader
			//_design.mLoading.gotoAndStop(1);
		}
		
		private function loadRankScreen_progress(mProgress:ProgressEvent):void
		{
			var percent:Number = mProgress.bytesLoaded/mProgress.bytesTotal;
			trace(percent);
		}
		
		
		private function btnShare_click(evt:MouseEvent):void
		{
			var streamTitle:String = LanguageManager.getInstance().getScoreTitleFeed();
			var streamText:String = LanguageManager.getInstance().getScoreTextFeed(facebookName, score);
			var streamMsg:String = LanguageManager.getInstance().getTranslation("CAN_YOU_DO_BETTER");
			
			GoApi.Instance.goFacebook.streamScore(streamTitle, streamText, streamMsg, rank);
			
			// TODO: only award when stream is confirmed
			// Save achievement step
			//if( !bSharedScore)
			//	Main.singletron.goApi.awardAchevementStep(1);
			
			bSharedScore = true;
		}
		
	}
}
