﻿package screens
{
	import com.screenmanager.Screen;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import game.*;
	
	import goapi.apis.datatypes.GoApiRankingType;
	import goapi.GoApi;
	import goapi.apis.GoApiAchievement;
	
	public class InstructionsScreen extends Screen
	{
		private var design:InstructionsScreen_design = new InstructionsScreen_design();
		
		public function InstructionsScreen()
		{
			super();
			addChild(design);
			
			// Select language
			var _currLanguage:uint = LanguageManager.getInstance().getLanguage();
			
			//design.instructionsTxt.text = LanguageManager.getInstance().getInstructions(LanguageManager.REFERENCE_INSTRUCTIONS_CLASSIC);
			design.txtKeys.text = LanguageManager.getInstance().getTranslation("VOCAB_KEYS_INSTRUCTION");
			
			if( Main.singletron._soundOn)
				design.video.volume = 0.7;
			else
				design.video.volume = 0.0;
			
			design.nextBtn.addEventListener(MouseEvent.CLICK, nextBtn_click)
			design.closeBtn.addEventListener(MouseEvent.CLICK, closeBtn_click);
			
			Main.singletron.gameApi.loadPermutation(12, 0, "");
			
			var rankTypeObj:GoApiRankingType = new GoApiRankingType();
			rankTypeObj.gameId = 1; // GoApi.Instance.apiBase.gameId;
			rankTypeObj.gameMode = 0;
			GoApi.Instance.goScore.loadRankingRanges(rankTypeObj);
			
			GoApiAchievement.Instance.getList();
		}
		
		public function preClose():void
		{
			design.nextBtn.removeEventListener(MouseEvent.CLICK, nextBtn_click)
			design.closeBtn.removeEventListener(MouseEvent.CLICK, closeBtn_click);
			design.video.volume = 0.0;
			design.video.stop();
			design.video = null;
		}
		
		private function nextBtn_click(evt:MouseEvent):void
		{
			preClose();
			
			Main.singletron.screenManager.LoadScreen(new CountDownScreen());
			//Main.singletron.screenManager.LoadScreen(new GameScreen());
			
			/*
			if (_gameMode < 10)
				showCountDownScreen();
			else
				displayVSPreScreen();
			*/
		}
		
		private function closeBtn_click(evt:MouseEvent):void
		{
			preClose();
			
			Main.singletron.screenManager.LoadScreen(new MenuScreen());
		}
		
	}
	
}
