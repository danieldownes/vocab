﻿package screens
{
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.display.Bitmap;
	import flash.utils.getTimer;
	import flash.utils.Timer;
	import flash.events.IOErrorEvent;
	import flash.utils.ByteArray;
	import goapi.GoApi;
	import screenpopups.AchievementPopup;
	import screenpopups.SignupPopup;
	
	import caurina.transitions.Tweener;
	
	import com.screenmanager.Screen;

	import game.Global;
	import game.LanguageManager;
	import screenpopups.YesNoPopup;
	import screenparts.*;

	
	public class GameScreen extends Screen
	{
		private var design:GameScreen_design = new GameScreen_design();
		private var mPopUp:YesNoPopup;
		
		public var wordsArr:Array;
		public var ans1Arr:Array;
		public var hideOnly:Boolean = false;
		
		private const GLOW_WAIT_TIMER:uint = 1000;
		private const WAIT_DISPLAY_BLOCKS:uint = 80;
		private const MAX_ROWS:uint = 10;
		private const BLOCK_GAP_X:uint = 0;
		private const BLOCK_GAP_Y:int = 0;
		private const BLOCK_GAP_WORD:uint = 7;
		private const GAME_OVER_TIME:uint =3*60000; // 2 minutes (2 * 60,000 milliseconds)
		//private const GAME_OVER_TIME:uint =10000; // 2 minutes (2 * 60,000 milliseconds)
		//private const SPEED_OF_LIGHT_TIME:uint = 4*60000
		private const RESET_TIME:uint = 20*1000;
		
		private var _answersArr:Array
		private var _selectedWords:String;
		private var _selectedWordInd:uint = 0;
		private var _resetStartingTime:uint;
		private var _startingTime:uint = 0;
		private var _rndPosArr:Array;
		private var _indPosArr:Array;
		private var _currentLetter:uint = 0;
		private var _targetPlacedArr:Array;
		private var _targetToBePlacedArr:Array;
		private var _score:uint = 0;
		private var _glowTimer:Timer
		private var _displayBlocksTimer:Timer;
		private var _displayedBlocks:uint = 0;
		private var _friendId:String;
		private var _imageLoader:Loader;
		private var _firstLoaded:Boolean = false;
		private var _oldTime:String
		private var _answersSolved:uint = 0;
		
		private var _permutationToUse:String;
		
		private var _iChangeWordCount:int = 0;
		private var _countCorrectAnswers:uint = 0;  // Counts number of correct answers since last word change.
		
		private var _wordsTot:uint = 0;		// Record the totel letters for first letter set
		private var _wordsFound:uint = 0;	// Record the found words for the first letter set
		
		private var _bGlow:Boolean = false;
		private var _storedKeyPresses:Array = new Array;
		private var _iKeyPresses:uint = 0;
		private var _currLanguage:uint = 0;
		
		
		private var _sndClock10secs:Sound;
		private var _channelClock10secs:SoundChannel = new SoundChannel();
		
		public function GameScreen()
		{
			super();
			addChild(design);
			
			design.scoreTxt.text = "0";
			design.txtRank.text = GoApi.Instance.goScore.getRank(_score).toString();
			
			_glowTimer = new Timer(GLOW_WAIT_TIMER);
			_glowTimer.addEventListener(TimerEvent.TIMER, onGlowWaitTimer);
			
			// Vs. mode
			//design.timer2Mc.visible = false;
			//design.displayPlayersImages(_goApi.loaderVars.facebookId, _friendId);
			_permutationToUse = Main.singletron.gameApi.vocabWord;
			
			_sndClock10secs = new snd_10secs();
			
			//Main.singletron._gameMode;
			_targetPlacedArr = new Array();
			_targetToBePlacedArr = new Array();
			
			// Set up words and permutation
			updateWord();
			
			// Init 'correct word' letters as invisible
			var tempMc:MovieClip = design.mCorrectWord['Mc'];
			for (var j:int = 0 ; j < 7; j++)
				tempMc['mL' + j].visible = false;
			
			
			// Save the total for the first set of words
			_wordsTot = design.blocks.numChildren;
			trace("setup _wordsTot=" + _wordsTot);
			
			// Select language
			_currLanguage = LanguageManager.getInstance().getLanguage();
			trace("game setup lang:" + _currLanguage);
			design.gotoAndStop(_currLanguage + 1);
			
			
			// Prepair UI ...
			
			// Disable 'change word' button
			design.changeWordBtn.mouseEnabled = false;
			design.changeWordBtn.mouseChildren = false;
			design.changeWordBtn.visible = true;
			design.changeWordBtn.gotoAndStop(2);
			design.changeWordBtn.addEventListener(MouseEvent.CLICK, onChangeWordClicked);
			
			design.closeBtn.visible = true;
			design.closeBtn.addEventListener(MouseEvent.CLICK, closeBtn_click);
			design.shuffleBtn.addEventListener(MouseEvent.CLICK, shuffleLetterPositions)
			design.backSpaceBtn.addEventListener(MouseEvent.CLICK, backSpaceBtnClicked);
			design.checkAnswer.addEventListener(MouseEvent.CLICK, checkAnswerClicked);
			design.addEventListener(Event.ENTER_FRAME, checkTimer);
			
			
			//design.mPlayMc.visible = false;
			//design.timer2Mc.visible = false;
			design.nextBtn.visible = false;
			design.nextBtn.addEventListener(MouseEvent.CLICK, nextBtn_click);
			
			Main.singletron.stage.addEventListener(KeyboardEvent.KEY_UP, checkKeyDown);
			Main.singletron.stage.focus = this;
		}
		
		
		private function closeBtn_click(e:MouseEvent):void 
		{
			mPopUp = new YesNoPopup("CONCEDE MATCH?");
			Main.singletron.screenManager.LoadScreen( mPopUp);
			mPopUp._design.btnYes.addEventListener(MouseEvent.CLICK, endGame_btnYes_click);
		}
		
		private function nextBtn_click(e:MouseEvent):void
		{
			stopDisplayBlocksTimer(); 
			//getTimer() - _startingTime, _selectedWords,
			Main.singletron.score = _score;
			Main.singletron.wordsTot = _wordsTot
			Main.singletron.wordsFound = _wordsFound;
			
			trace("guest play:" + GoApi.Instance.apiBase.guestPlay);
			if( GoApi.Instance.apiBase.guestPlay == false)
				Main.achievements.check();
			else
			{
				Main.singletron.screenManager.LoadScreen(new ScoreScreen());
				Main.singletron.screenManager.LoadScreen(new SignupPopup());
			}
		}
		
		public function updateWord():void
		{
			// Set current words (they should have already been loaded)
			wordsArr = new Array( Main.singletron.gameApi.vocabWord);
			ans1Arr = new Array( Main.singletron.gameApi.vocabPermutations);
			_answersArr = new Array(ans1Arr);
			
			// Load next set of permutations, ready for next change
			Main.singletron.gameApi.loadPermutation(_currLanguage, 0, "");
			
			_answersSolved = 0;
			_targetPlacedArr = new Array();
			_targetToBePlacedArr = new Array();
			_answersArr = new Array(ans1Arr);
			
			// Setup game MCs
			placeLetters();
			placeBlockLetters();
		}
		
		public function displayPlayersImages(id1:String, id2:String):void
		{
			design.loadMeImage(id1);
			design.loadFriendImage(id2);
		}
		
		
		private var image1Loader:Loader;
		private var image2Loader:Loader;
		/*
		public function loadMeImage(id:String ):void
		{
			trace("load me pic:" + id);
			
			try
			{
				url1Request = new URLRequest("facebookProxy.php");
				var variables:URLVariables = new URLVariables();
				variables.path = "http://graph.facebook.com/" + id + "/picture?type=square";
				url1Request.data = variables;
				
				url1Loader = new URLLoader();
				url1Loader.dataFormat = URLLoaderDataFormat.BINARY;
				url1Loader.addEventListener(Event.COMPLETE, completeHandlerMe);
				url1Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				url1Loader.load(url1Request);
			}catch(e:Error)
			{
				trace("problem loading mePic:" + e.message);
			}
		}
		
		
		private function completeHandlerMe(event:Event):void
		{
			try
			{
				var byteArray:ByteArray = url1Loader.data;

				url1Loader.removeEventListener(Event.COMPLETE, completeHandlerMe);
				
				image1Loader = new Loader();
				image1Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, meImageLoaded);
				image1Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				image1Loader.loadBytes(byteArray);
			}catch(e:Error)
			{
			}
		}
		
		private function meImageLoaded(e:Event):void
		{
			var image:Bitmap = new Bitmap(e.target.content.bitmapData);
			image.smoothing = true;
			design.mPlayMc.player1Mc.imageMc.removeChildAt(0)
			design.mPlayMc.player1Mc.imageMc.addChild(image);
		}
		
		
		public function loadFriendImage( id:String ):void
		{
			try
			{
				url2Request = new URLRequest("facebookProxy.php");
				var variables:URLVariables = new URLVariables();
				variables.path = "http://graph.facebook.com/" + id + "/picture?type=square";
				url2Request.data = variables;
				
				url2Loader = new URLLoader();
				url2Loader.dataFormat = URLLoaderDataFormat.BINARY;
				url2Loader.addEventListener(Event.COMPLETE, completeHandlerFriend);
				url2Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				url2Loader.load(url2Request);
			}catch(e:Error)
			{
			}
		}
		
		private function completeHandlerFriend(event:Event):void
		{
			try
			{
				var byteArray:ByteArray = url2Loader.data;
				
				url2Loader.removeEventListener(Event.COMPLETE, completeHandlerFriend);
				
				image2Loader = new Loader();
				image2Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, friendImageLoaded);
				image2Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				image2Loader.loadBytes(byteArray);
			}catch(e:Error)
			{
			}
		}
		
		private function friendImageLoaded(e:Event):void
		{
			var image:Bitmap = new Bitmap(e.target.content.bitmapData);
			design.mPlayMc.player2Mc.imageMc.removeChildAt(0)
			design.mPlayMc.player2Mc.imageMc.addChild(image);
		}
		
		
		private function onIOError(evt:IOErrorEvent):void
		{
			//_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		*/
		
		
		public function onChangeWordClicked(evt:MouseEvent = null, forceReset:Boolean = false):void
		{
			_iChangeWordCount++;
			
			_resetStartingTime = getTimer();
			var len:uint = _targetPlacedArr.length
			for(var j:uint = 0 ; j < len; j++)
				backSpaceBtnClicked(null, false);
				
			removeLetters();
			removeBlockLetters();
			
			updateWord();
			
			// Subtract time
			addTimeBonus(-5);
			
			// Re-disable button
			design.changeWordBtn.gotoAndStop(2);
			design.changeWordBtn.mouseEnabled = false;
		}
		
		private function checkTimer(evt:Event = null):void
		{
			if (_startingTime == 0)
			{
				_startingTime = getTimer();
				_resetStartingTime = getTimer();
			}
			
			// Time's up?
			if( getTimer() - _startingTime < GAME_OVER_TIME)
				design.timeLeftTxt.text = converToTime(GAME_OVER_TIME - (getTimer() - _startingTime));
			else
				itsGameOver(true);
			
		}
		
		public function itsGameOver( bClearTime:Boolean):void
		{
			if( bClearTime)
				design.timeLeftTxt.text = "00:00";
			
			endGameClicked();
		}
		
		private function endGameClicked(evt:MouseEvent = null):void
		{
			_displayBlocksTimer = new Timer(WAIT_DISPLAY_BLOCKS);
			_displayedBlocks = 0;
			_displayBlocksTimer.start();
			_displayBlocksTimer.addEventListener(TimerEvent.TIMER, onDisplayBlocksTimer);
			
			removeListeners();
			//mPopUp.ScreenExit();
			design.closeBtn.visible = false;
			
			design.nextBtn.visible = true;
			
			Main.singletron.score = _score;
		}
		
		
		private function onDisplayBlocksTimer(evt:TimerEvent = null):void
		{
			if (_displayedBlocks < design.blocks.numChildren)
			{
				var blockMc:MovieClip = design.blocks.getChildAt(_displayedBlocks++) as MovieClip;
				var bMc:BlockLetter = blockMc.getChildAt(0) as BlockLetter;
				if (bMc.letterTxt.visible == false)
				{
					for (var i:uint = 0 ; i < blockMc.numChildren; i++)
					{
						bMc = blockMc.getChildAt(i) as BlockLetter;
						bMc.letterTxt.visible = true
					}
				}else
				{
					onDisplayBlocksTimer();
				}
			}else
			{
				_displayBlocksTimer.stop();
				_displayBlocksTimer.removeEventListener(TimerEvent.TIMER, onDisplayBlocksTimer);
			}
		}
		
		public function stopDisplayBlocksTimer():void
		{
			_displayBlocksTimer.stop();
			
			if( _displayBlocksTimer.hasEventListener(TimerEvent.TIMER))
				_displayBlocksTimer.removeEventListener(TimerEvent.TIMER, onDisplayBlocksTimer);
		}

		private function removeListeners():void
		{
			design.shuffleBtn.alpha = .5;
			design.backSpaceBtn.alpha = .5;
			design.checkAnswer.alpha = .5;
			design.changeWordBtn.alpha = .5;
			
			design.shuffleBtn.removeEventListener(MouseEvent.CLICK, shuffleLetterPositions)
			design.backSpaceBtn.removeEventListener(MouseEvent.CLICK, backSpaceBtnClicked);
			design.checkAnswer.removeEventListener(MouseEvent.CLICK, checkAnswerClicked);
			design.removeEventListener(Event.ENTER_FRAME, checkTimer);
			design.changeWordBtn.removeEventListener(MouseEvent.CLICK, onChangeWordClicked)
			
			design.shuffleBtn.enabled = false;
			design.backSpaceBtn.enabled = false;
			design.checkAnswer.enabled = false;
			design.changeWordBtn.enabled = false;
			
			for (var i:uint = 0; i < _selectedWords.length; i++)
			{
				var letterMc:LetterMc = design.getChildByName("letterMc"+i) as LetterMc;
				letterMc.enabled = false;
				letterMc.removeEventListener(MouseEvent.CLICK, onLetterPress)
			}
		}
		
		private function converToTime(time:uint, onlySec:Boolean = false):String
		{
			var min:String;
			if(time%60000 > 60000 - 1000)
				min = String(Math.ceil(time/60000));
			else
				min = String(Math.floor(time / 60000));
			
			var sec:String = String(Math.ceil((time%60000)/1000)%60);
			
			if( onlySec == false)
			{
				if( sec != _oldTime && Number(sec) >= 0 )
				{
					_oldTime = sec;
					//var snd:Sound;
					
					if (min == "0" && Number(sec) == 10 && Main.singletron._soundOn )
					{
						Main.singletron.PlaySound(_sndClock10secs);
						//trace("play _sndClock10secs");
					}
					
					//	snd = new Clock1SecSnd();
				}
			}
			
			if(min.length ==1)
				min = "0"+min;
			
			if(sec.length == 1)
				sec = "0"+sec;
			
			if(onlySec == true)
				return sec;
			
			return min+":"+sec;
		}
		
		public function matchKeyWord(char:String):void
		{
			for( var i:uint = 0; i < _targetToBePlacedArr.length; i++)
			{
				if( _targetToBePlacedArr[i] != null
					&& _targetToBePlacedArr[i].letterTxt.text == char
					&& _targetToBePlacedArr[i].addedToList == false)
				{
					_targetToBePlacedArr[i].dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				}
			}
		}
		
		private function removeLetters():void
		{
			for (var i:uint = 0; i < _selectedWords.length; i++)
			{
				var letterMc:LetterMc = design.getChildByName("letterMc"+i) as LetterMc;
				letterMc.removeEventListener(MouseEvent.CLICK, onLetterPress);
				design.removeChild(letterMc);
			}
		}
		
		
		private function placeLetters():void
		{
			_selectedWords = shuffle(wordsArr[_selectedWordInd]);
			for (var i:uint = 0; i < _selectedWords.length; i++)
			{
				var letterMc:LetterMc = new LetterMc();
				letterMc.setup(_selectedWords.charAt(i));
				var letterOutlineMc:MovieClip = design.getChildByName("letterOutiline"+String(i+1)) as MovieClip;
				letterMc.name = "letterMc"+i;
				letterMc.ind = i;
				letterMc.x = letterOutlineMc.x + (letterOutlineMc.width - letterMc.width)/2;
				letterMc.y = letterOutlineMc.y + (letterOutlineMc.height - letterMc.height)/2;
				letterMc.buttonMode = true;
				letterMc.mouseChildren = false;
				letterMc.addEventListener(MouseEvent.CLICK, onLetterPress)
				_targetToBePlacedArr.push(letterMc);
				design.addChild(letterMc);
			}
		}
		
		private function removeBlockLetters():void
		{
			var len:uint = design.blocks.numChildren;
			for( var i:uint = 0; i < len; i++)
				design.blocks.removeChild(design.blocks.getChildAt(0));
		}
		
		private function placeBlockLetters():void
		{
			var selectedStr:String = _answersArr[_selectedWordInd];
			var selectedArr:Array = selectedStr.split(",");
			var rows:uint = 0;
			var oldLength:uint;
			var oldBlock:MovieClip;
			for(var i:uint = 0; i < selectedArr.length; i++)
			{
				var blockLetters:MovieClip = new MovieClip();
				blockLetters.name = "block"+i;
				addBlockLetters(blockLetters,selectedArr[i]);
				if(i != 0){
					if(oldLength != selectedArr[i].length){
						rows = 0;
						blockLetters.x =  (oldBlock.x + oldBlock.width + BLOCK_GAP_WORD)
					} else if(rows == MAX_ROWS - 1){
						rows = 0;
						blockLetters.x =  (oldBlock.x + oldBlock.width + BLOCK_GAP_WORD)
					} else{
						rows++;
						blockLetters.x = (oldBlock.x)
					}
				}
				blockLetters.y = rows * (blockLetters.height + BLOCK_GAP_Y)
				oldBlock = blockLetters
				design.blocks.addChild(blockLetters);
				oldLength = selectedArr[i].length;
			}
		}
		
		public function shuffleLetterPositions(evt:MouseEvent = null):void
		{
			Main.singletron.PlaySound(new snd_shuffle);
			
			_selectedWords = shuffle(wordsArr[_selectedWordInd])
			//trace(_rndPosArr);
			var newTargetToBePlacedArr:Array = new Array();
			for (var i:uint = 0; i < _selectedWords.length; i++)
			{
				if (_targetToBePlacedArr[i] != null)
				{
					var shufleMc:MovieClip = _targetToBePlacedArr[i] as MovieClip;
					var targetMc:MovieClip = design.getChildByName("letterOutiline"+(_rndPosArr[i]+1)) as MovieClip;
					newTargetToBePlacedArr[_rndPosArr[i]] = shufleMc
					shufleMc.ind = _rndPosArr[i];
					Tweener.addTween(shufleMc,{x:targetMc.x + (targetMc.width - shufleMc.width)/2,time:1});
				}else
					newTargetToBePlacedArr[_rndPosArr[i]] = null;
			}
			_targetToBePlacedArr = newTargetToBePlacedArr;
		}
		
		private function addBlockLetters(mc:MovieClip, str:String):void
		{
			for (var i:uint = 0; i < str.length; i++)
			{
				var blockMc:MovieClip = new BlockLetter();
				blockMc.setup(str.charAt(i));
				blockMc.x = blockMc.width * i;
				mc.addChild(blockMc);
			}
		}
		
		public function checkAnswerClicked(evt:MouseEvent = null):void
		{
			// If not enough letters entered, then do nothing..
			if( _targetPlacedArr.length <= 3)
				return;
				
			var word:String = "";
			for(var i:uint = 0; i < _targetPlacedArr.length; i++)
				word += _targetPlacedArr[i].letterTxt.text;
			
			var selectedStr:String = _answersArr[_selectedWordInd];
			var selectedArr:Array = selectedStr.split(",");
			var found:String = "no";
			for( i = 0; i < selectedArr.length; i++)
			{
				if (selectedArr[i] == word)
				{
					var selectedBlock:MovieClip = design.blocks.getChildByName("block"+i) as MovieClip;
					for( var j:uint = 0; j < selectedBlock.numChildren; j++)
					{
						var block:BlockLetter = selectedBlock.getChildAt(j) as BlockLetter;
						if(block.finded == false){
							found = "yes";
							block.showLetter()
						} else{
							found = "";
							break;
						}
					}
					break;
				}
			}
			
			var len:uint =  _targetPlacedArr.length
			var snd:Sound;
			
			if (found == "yes")
			{
				// Play effects ...
				Main.singletron.PlaySound(new CorrectWordSnd);
				
				// 'Correct word letters' (new) glow effect
				for (j = 0 ; j < 7; j++)
				{
					if ( j < len)
					{
						// Prepare 'correct word' letter
						design.mCorrectWord.Mc['mL' + j].visible = true;
						design.mCorrectWord.Mc['mL' + j].letterTxt.text = _targetPlacedArr[j].letterTxt.text;
						design.mCorrectWord.Mc['mL' + j].gotoAndPlay(2);
					}else
						design.mCorrectWord.Mc['mL' + j].visible = false;
				}
				design.mCorrectWord.gotoAndPlay(2);
				design.mCorrectWord.visible = true;
				
				// Control when 'change word' is to be enabled
				_countCorrectAnswers++;
				if( _countCorrectAnswers >= 5)
				{
					_countCorrectAnswers = 0;
					design.changeWordBtn.mBoarder.gotoAndPlay(2);
					design.changeWordBtn.gotoAndStop(1);
					design.changeWordBtn.mouseEnabled = true;
				}
				
				_answersSolved++;
				//trace(_answersSolved, " vs " , selectedArr.length)
				
				// Save found words for first letter set only (used for ranking in classic mode)
				if( _iChangeWordCount == 0 )
					_wordsFound = _answersSolved;
				
				// Calc score and remove letters
				for (j = 0 ; j < len; j++)
				{
					_score += 25 + j;
					
					// Remove actual/exisiting letter
					backSpaceBtnClicked(null, false);
				}
				
				design.scoreTxt.text = String(_score);
				var rank:int = GoApi.Instance.goScore.getRank(_score);
				design.txtRank.text = rank.toString();
				
				// Add time bonus (based on rank)
				var aAddTime:Array = new Array(0,0,1,1,2,2,2,3,3,4,4,5);
				addTimeBonus(aAddTime[rank - 1]);
				
				// If all the permutations were found then...
				if( _answersSolved == selectedArr.length)
				{
					// Game completed
					onChangeWordClicked(null, true);
					
					// Add time bonus
					addTimeBonus(10);
					
					//if( gameMode == "normal" )
					//	endGameClicked();
					//else
				}
				
				// Mark Word Length Achievement
				Main.achievements.arrayFoundWordLength[len - 4] = true;
				
			}else if(found == "no")
			{
				Main.singletron.PlaySound(new ErrorWordSnd);
				
				for(j = 0 ; j < len; j++)
					backSpaceBtnClicked(null, false);
				
			}else
			{
				//alreadyMc.play()
				for(j = 0 ; j < len; j++)
					backSpaceBtnClicked(null, false);
			}
		}
		
		private function addTimeBonus(iAmount:int):void
		{
			var oldTime:int = Math.ceil((GAME_OVER_TIME - (getTimer() - _startingTime)) / 1000) - 1;
			_startingTime += iAmount * 1000;
			
			// If there was less than 10 seconds left on the clock, then rewind the clock sound accourdingly
			if( Main.singletron._soundOn )
			{
				// First, Stop the sound, if it was playing
				if ( oldTime <= 10 && _channelClock10secs != null)
					_channelClock10secs.stop();
					
				var secs:int = Math.ceil((GAME_OVER_TIME - (getTimer() - _startingTime)) / 1000) - 1;
				if ( secs <= 10)
				{
					_channelClock10secs = _sndClock10secs.play( (10 - secs) * 3000);
					//trace("play _sndClock10secs :" + 30 * (10 - secs));
				}
				//_startingTime
			}
			
			// Show addTime indicator
			design.addTime.play();
			
			if( iAmount > 0)
				design.addTime.mc.txt.text = "+" + iAmount.toString();
			else
				design.addTime.mc.txt.text = iAmount.toString();
		}
		
		private function onGlowWaitTimer(evt:TimerEvent):void
		{
			_glowTimer.stop();
			_bGlow = false;
			
			// Remove exisiting letters
			var len:uint =  _targetPlacedArr.length;
			for(var j:uint = 0 ; j < len; j++)
				backSpaceBtnClicked(null, false);
			
			// Re-add any stored keypresses which may have been entered during the glow
			for( var n:uint = 0; n < _iKeyPresses; n++)
				matchKeyWord(_storedKeyPresses[n]);
			_iKeyPresses = 0;
			
			// Clear the array
			_storedKeyPresses.length = 0;
		}
		
		private function endGame_btnYes_click(e:MouseEvent):void 
		{
			Main._STAGE.removeEventListener(KeyboardEvent.KEY_UP, checkKeyDown);
			
			endGameClicked();
		}
		
		public function backSpaceBtnClicked(evt:MouseEvent = null, bol:Boolean = true):void
		{
			if (_targetPlacedArr.length != 0)
			{
				var letterMc:LetterMc = _targetPlacedArr[_targetPlacedArr.length - 1]
				moveLetterBack(letterMc, bol);
			}
		}
		private function onLetterPress(evt:MouseEvent):void
		{
			var letterMc:LetterMc = evt.currentTarget as LetterMc;
			
			if (letterMc.animation == false)
			{
				var targetMc:MovieClip
				if (letterMc.addedToList == false)
				{
					Main.singletron.PlaySound(new SelectLetterSnd1);
					
					letterMc.animation = true;
					_targetPlacedArr.push(letterMc);
					letterMc.placedInd = _targetPlacedArr.length - 1;
					letterMc.addedToList = true;
					targetMc = design.getChildByName("targetOutLine"+(_currentLetter+1)) as MovieClip;
					letterMc.moveToPos(targetMc.x + (targetMc.width - letterMc.width)/2,targetMc.y + (targetMc.height - letterMc.height)/2);
					_targetToBePlacedArr[letterMc.ind] = null;
					_currentLetter++;
				}else
				{
					moveLetterBack(letterMc);
				}
			}
		}
		
		private function moveLetterBack(letterMc:LetterMc, sndBol:Boolean = true):void
		{
			if( sndBol)
				Main.singletron.PlaySound(new RemoveLetterSnd1);
			
			letterMc.addedToList = false;
			var targetMc:MovieClip;
			for (var i:uint = 0; i < _targetToBePlacedArr.length; i++)
			{
				if (_targetToBePlacedArr[i] == null)
				{
					targetMc = design.getChildByName("letterOutiline"+(i+1)) as MovieClip;
					if( letterMc.checkTweening())
						letterMc.removeTweening();
					
					letterMc.x = targetMc.x + (targetMc.width - letterMc.width)/2
					letterMc.y = targetMc.y + (targetMc.height - letterMc.height)/2
					_targetToBePlacedArr[i] = letterMc;
					letterMc.ind = i;
					
					break;
				}
			}
			
			_targetPlacedArr.splice(letterMc.placedInd,1);
			for (var j:uint = letterMc.placedInd ; j < _targetPlacedArr.length; j++)
			{
				var moveLetter:LetterMc = _targetPlacedArr[j];
				
				targetMc = design.getChildByName("targetOutLine"+(j+1)) as MovieClip;
				moveLetter.moveToPos(targetMc.x + (targetMc.width - moveLetter.width)/2,targetMc.y + (targetMc.height - moveLetter.height)/2);
				moveLetter.placedInd = j;
			}
			_currentLetter--;
		}
		
		private function shuffle(str:String):String
		{
			var str1:String = "";
			var len:uint = str.length;
			_rndPosArr = new Array();
			_indPosArr = new Array(0,1,2,3,4,5,6,7)
			for (var i:uint = 0; i < len; i++)
			{
				var rnd:uint = Math.floor(Math.random()*str.length)
				_rndPosArr.push(_indPosArr[rnd]);
				_indPosArr.splice(rnd,1)
				str1 += str.charAt(rnd);
				str = removeCharAtInd(str,rnd);
			}
			return str1;
		}
		
		private function removeCharAtInd(str:String, ind:uint):String
		{
			var newStr:String = "";
			for (var i:uint = 0; i < str.length; i++)
			{
				if(i != ind)
					newStr += str.charAt(i);
			}
			return newStr;
		}
		
		
		private function checkKeyDown(evt:KeyboardEvent):void
		{
			if(evt.keyCode >= 65 && evt.keyCode <= 90)
				matchKeyWord((String.fromCharCode( evt.charCode ).toUpperCase()));
			else if(evt.keyCode == 8)
				backSpaceBtnClicked()
			else if(evt.keyCode == 13)
				checkAnswerClicked();
			else if( evt.keyCode == 17 && design.changeWordBtn.mouseEnabled)
				design.changeWordBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK))
			else if( evt.keyCode == 32)
				shuffleLetterPositions();
		}
		
	}
}
