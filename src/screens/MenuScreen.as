﻿package screens
{
	import flash.display.MovieClip;
	import flash.media.Sound;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import goapi.apis.GoApiAchievement;
	
	import com.screenmanager.Screen;
	
	import events.SelectLanguage;
	import events.LoadSwf;
	import game.*;
	import goapi.*;
	
	import screens.*;
	import screenpopups.*;
	
	
	public class MenuScreen extends Screen
	{
		private var design:MenuScreen_design = new MenuScreen_design;
		
		private const SCREEN_WIDTH:uint = 700;
		private const SCREEN_HEIGHT:uint = 530;
		private const GAP:uint = 60;
		
		private const ANIMATION_TIME:Number = 0.6;
		
		
		//_menuScreen.setup(currLanguage, _globalVars, _goApi, _goApi.loaderVars.goGamePermissions);
		
		public function MenuScreen() // _selectedLanguage:uint, globalVars:Global, _goApi:GoApi, permissions:String)
		{
			super();
			addChild(design);
			
			try
			{
				design.txtVersion.text = "Game Odyssey v1 r" + GoApi.Instance.apiBase.loaderVars.releaseNum;
			}catch(e:Error)
			{
			}
			
			var _selectedLang:uint = LanguageManager.getInstance().getLanguage();
			
			//TODO: design.langMc.txt.text = LanguageManager.getInstance().languageArr[_selectedLang];
			design.langMc.mouseChildren = false;
			design.langMc.buttonMode = true;
			
			//updateLanguage();
			
			Main.singletron.addEventListener(SelectLanguage.SELECT_LANGUAGE, onSelectLanguage);
			
			//addEventListener(LoadSwf.LOAD_SWF, Vocabularious.main.loadExternalSwf);
			
			
			// Check user's permisions
			
			// Enable shop?
			//if( permissions == "shop")
			//	design.mDisableShop.visible = false;
			
			// Menu
			design.mcMenuButton1.addEventListener(MouseEvent.MOUSE_OVER, doMouseOver);
			//design.mcMenuButton2.addEventListener(MouseEvent.MOUSE_OVER, doMouseOver);
			design.mcMenuButton1.addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
			//design.mcMenuButton2.addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
			
			// TODO: Stage 1
			design.mcMenuButton2.alpha = 0.3;
			design.mcMenuButton2.mouseEnabled = false;
			design.inviteBtn.alpha = 0.3;
			design.inviteBtn.mouseEnabled = false;
			design.mShop.alpha = 0.3;
			design.mShop.mouseEnabled = false;
			design.btnGift.alpha = 0.3;
			design.btnGift.mouseEnabled = false;
			
			
			design.btnProfile.addEventListener(MouseEvent.CLICK, btnProfile_click);
			
			if( GoApi.Instance.apiBase.guestPlay)
			{
				design.btnProfile.alpha = 0.3;
				design.btnProfile.mouseEnabled = false;
			}
			
			/*
			design.langMc.addEventListener(MouseEvent.CLICK, btnChangeLanguage_click);
			design.btnMates.addEventListener(MouseEvent.CLICK, btnMates_click);
			design.btnGift.addEventListener(MouseEvent.CLICK, btnGift_click);
			design.inviteBtn.addEventListener(MouseEvent.CLICK, btnInvite_click);
			//selectLangBtn.addEventListener(MouseEvent.CLICK, selectLangBtnClicked);
			
			design.mLogo.mouseEnabled = false;
			
			*/
			//design.langMc.addEventListener(MouseEvent.CLICK, btnChangeLanguage_click);
		}
		
		
		/*
		public function updateLanguage():void
		{
			design.txtGameLang.text = LanguageManager.getInstance().getTranslation("GAME_LANGUAGE");
			
			// Menu buttons
			for (var i:Number = 0; i < _arrMenuButtonLabels[0].length; i++)
			{
				var mc:MovieClip = design.getChildByName("mcMenuButton"+i) as MovieClip;
				mc.txtLabel.text = _arrMenuButtonLabels[Main.singletron.selectedLang][i];
			}
		}
		*/
		
		private function doMouseOver(e:MouseEvent):void
		{
			Main.singletron.PlaySound(new MenuSnd1);
		}
		
		
		private function onSelectLanguage(evt:SelectLanguage):void
		{
			//trace("Language Changed");
			LanguageManager.getInstance().setLanguage( evt._languageId );
			
			design.langMc.txt.text = DictionaryManager.getInstance().getDictionaryList()[evt._languageId]._name;
			
			//updateLanguage();
			
			trace("onSelectLanguage:" + evt._languageId );
			/*
			//var mc:MovieClip = evt.currentTarget as MovieClip;
			_selectedLang = newLanguageId;
			this.dispatchEvent(new SelectLanguage(_selectedLang,true));
			
			design.allLangScreen.bgMc.closeBtn.removeEventListener(MouseEvent.CLICK, bgMcClicked)
			design.allLangScreen.bgMc.removeEventListener(MouseEvent.CLICK, bgMcClicked)
			
			*/
			
			// Update bottom Facebook screen
			//this.dispatchEvent(new SelectLanguage(evt._languageId));
		}
		
		
		private function btnChangeLanguage_click(e:MouseEvent):void
		{
			Main.singletron.screenManager.LoadScreen(new LanguageScreenPopup());
		}
		
		private function doMouseDown(e:MouseEvent):void
		{
			Main.singletron.PlaySound(new MenuClickSnd);
			
			if (e.target.name == "mcMenuButton1")
			{
				Main.singletron.setGameMode(Global.GAME_MODE_CLASSIC);
				Main.singletron.screenManager.LoadScreen(new InstructionsScreen());
			}
			else if (e.target.name == "mcMenuButton2")
			{
				Main.singletron.setGameMode(Global.GAME_MODE_CLASSIC_VERSUS);
				Main.singletron.screenManager.LoadScreen(new ChallengeScreen());
			}
			
		}
		
		private function btnProfile_click(evt:MouseEvent):void
		{
			Main.singletron.PlaySound(new MenuClickSnd);
			navigateToURL(new URLRequest(GoApi.Instance.BASE_URL + "profile/"), "_blank");
		}
		
		private function btnMates_click(evt:MouseEvent):void
		{
			this.dispatchEvent(new LoadSwf(Global.MATES_SWF, true));
			//design.mLoadingProfile.gotoAndStop(2);
		}
		
		private function btnGift_click(evt:MouseEvent):void
		{
			Main.singletron.PlaySound(new MenuClickSnd);
			navigateToURL(new URLRequest(GoApi.Instance.BASE_URL + "shop/"), "_blank");
		}
		
		private function btnInvite_click(evt:MouseEvent):void
		{
			var streamMsg:String = LanguageManager.getInstance().getInviteFeed();
			
			GoApi.Instance.goFacebook.inviteFriends(streamMsg);
			
			Main.singletron.PlaySound(new snd_invite);
		}
	}
}
