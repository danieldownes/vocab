﻿package screens
{
	import com.screenmanager.Screen;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	
	import goapi.*;
	import goapi.apis.events.ChallengeLeaderboardResult;
	
	import caurina.transitions.Tweener;
	
	import screenparts.*;
	import events.InviteFriend;
	import game.LanguageManager;

	public class ChallengeScreen extends Screen
	{
		private var _design:ChallengeScreen_design = new ChallengeScreen_design();
		
		private const GAP:uint = 5;
		
		private var _stepHeight:Number = 54.6;
		private var _visibleFriends:uint = 7;
		
		public var friendsArr:Array;
		
		private var _facebookId:String;
		private var _facebookName:String;
		private var a_me_friends:String;
		private var a_me_wins:String;
		private var a_me_loss:String;
		private var a_me_lastResult:String;
		private var _leaderboard:String;
		private var _requests:String;
		private var _results:String;
		
		public function ChallengeScreen()
		{
			super();
			addChild(_design);
			
			//GoApi.Instance.goChallenge.getLeaderboard();
			//GoApi.Instance.goChallenge.addEventListener(ChallengeLeaderboardResult.CHALLENGE_LEADERBOARD_RESULT, ChallengeLeaderboardResult_ready);
			
			// Read challenge history from vars
			if( Main._STAGE.loaderInfo.url.indexOf("file://") != 0 && GoApi.Instance.apiBase.loaderVars != null)
			{
				_facebookId = GoApi.Instance.apiBase.loaderVars.facebookId;
				_facebookId = GoApi.Instance.apiBase.loaderVars.facebookName;
				a_me_friends = GoApi.Instance.apiBase.loaderVars.goFriends;
				a_me_wins = GoApi.Instance.apiBase.loaderVars.gameWins0;
				a_me_loss = GoApi.Instance.apiBase.loaderVars.gameLoss0;
				a_me_lastResult = GoApi.Instance.apiBase.loaderVars.gameLastResult0;
				_leaderboard = GoApi.Instance.apiBase.loaderVars.gameLeadership0
				_requests = GoApi.Instance.apiBase.loaderVars.gameRequests
				_results = GoApi.Instance.apiBase.loaderVars.gameResults
			}
			
			_design.inviteBtn.addEventListener(MouseEvent.CLICK, inviteBtnClicked);
			_design.btnClose.addEventListener(MouseEvent.CLICK, btnClose_click);
			
			setup();
		}
		
		/*
		private function ChallengeLeaderboardResult_ready(e:ChallengeLeaderboardResult):void 
		{
			a_me_friends = e.leadership;
			a_me_result = e.lastResult;
			a_me_wins = e.wins;
			a_me_loss = e.losses;
			
			setup();
		}
		*/
		
		private function setup():void
		{
			var frd:FriendListMc = new FriendListMc();
			var friendsListMc:MovieClip = new MovieClip();
			
			// Update 'my' stats
			_design.txtLosses.text = a_me_loss;
			_design.txtWins.text = a_me_wins;
			frd.setup(_facebookId, _facebookName, Number(a_me_wins), Number(a_me_loss), _results, true);
			
			// Add avialable_friends (eg friends that also have this game)
			var str:String = a_me_friends;
			var frdArr:Array = a_me_friends.split(".");
			var str1:String;
			
			//trace( "frdArr.length=" + frdArr.length);
			
			if( str != "")
			{
				for (var i:uint = 0; i < frdArr.length; i++)
				{
					frd = new FriendListMc();
					str1 = frdArr[i];
					var eachFrdArr:Array = str1.split("=");
					frd.setup(eachFrdArr[0],eachFrdArr[1], eachFrdArr[2],eachFrdArr[3],eachFrdArr[4]);
					frd.y = (frd.height + GAP) * i;
					frd.btnChallenge.addEventListener(MouseEvent.CLICK, btnChallenge_click);
					friendsListMc.addChild(frd);
				}
				_design.scrollerMc.containerMc.addChild(friendsListMc);
				_design.scrollerMc.containerMc.steps = 0;
				if( friendsListMc.height <= _design.scrollerMc.maskMc.height)
				{
					_design.scrollerMc.upBtn.visible = false;
					_design.scrollerMc.downBtn.visible = false;
				}else
				{
					_design.scrollerMc.containerMc.totalSteps = _visibleFriends - i;
					_design.scrollerMc.upBtn.alpha = .3;
					_design.scrollerMc.upBtn.addEventListener(MouseEvent.CLICK, moveUp)
					_design.scrollerMc.downBtn.addEventListener(MouseEvent.CLICK, moveDown)
				}
			}else
			{
				/*
				iGoto = LanguageManager.getInstance().getLanguage();
				iGoto = iGoto * 10 + 3
				_design.gotoAndStop( iGoto);
				*/
				_design.gotoAndStop(1);
			}
		}
		
		
		private function moveDown(evt:MouseEvent):void
		{
			if( _design.scrollerMc.containerMc.steps > _design.scrollerMc.containerMc.totalSteps)
			{
				_design.scrollerMc.containerMc.steps--;
				if( _design.scrollerMc.containerMc.steps == _design.scrollerMc.containerMc.totalSteps)
					_design.scrollerMc.downBtn.alpha = .3;
					
				_design.scrollerMc.upBtn.alpha = 1;
				Tweener.addTween(_design.scrollerMc.containerMc,{y:_design.scrollerMc.containerMc.steps * _stepHeight,time:.6});
			}
		}
		private function moveUp(evt:MouseEvent):void
		{
			if (_design.scrollerMc.containerMc.steps < 0)
			{
				_design.scrollerMc.containerMc.steps++;
				if(_design.scrollerMc.containerMc.steps == 0)
					_design.scrollerMc.upBtn.alpha = .3;
				
				_design.scrollerMc.downBtn.alpha = 1;
				Tweener.addTween(_design.scrollerMc.containerMc,{y:_design.scrollerMc.containerMc.steps * _stepHeight,time:.6});
			}
		}
		
		private function btnChallenge_click(evt:MouseEvent):void
		{
			trace("btnChallenge_click");
			/*
			var frd:FriendListMc = evt.currentTarget as FriendListMc;
			this.dispatchEvent(new StartMultipleGame(frd.id, "","", 0, -1, "", "", true));
			*/
			Main.singletron.screenManager.LoadScreen(new VsPreScreen());
		}
		
		private function inviteBtnClicked(evt:MouseEvent):void
		{
			// built-in invite box
			//DD this.dispatchEvent(new ShowInviteBtnScreen(true));
			
			// Exit now if not in a browser
			if( Main._STAGE.loaderInfo.url.indexOf("file://"))
				return;
			
			var streamText:String = LanguageManager.getInstance().getInviteFeed();
			
			trace( "calling newInviteChallenge");
			
			/*
			goapiFacebook.Instace().invite();
			
			// TODO: Call Facebook's invite screen
			try
			{
				ExternalInterface.call("newInviteChallenge('" + streamText + "')");
			}catch(e:Error)
			{ }
			ExternalInterface.addCallback("jsMsgShowNext", jsMsgShowNext);
			trace("ExternalInterface.addCallback jsMsgShowNext");
			*/
			
			
			Main.singletron.PlaySound(new snd_invite);
		}
		
		
		private function btnClose_click(e:MouseEvent):void 
		{
			Main.singletron.PlaySound(new MenuClickSnd);
			
			Main.singletron.screenManager.LoadScreen( new MenuScreen());
		}
	}
}
