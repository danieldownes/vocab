﻿package screens
{
	
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.display.Loader;
	import flash.events.MouseEvent;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	import com.screenmanager.Screen;
	
	
	public class VsPreScreen extends Screen
	{
		private var _design:VsPreScreen_design = new VsPreScreen_design();
		
		private var _imageLoader:Loader;
		private var _imageLoader2:Loader;
		//private var ldrContent:Bitmap;
		//private var ldr2Content:Bitmap;
		
		//private var bSound:Boolean;			// Used in fla file (now declared there)
		
		public function VsPreScreen()
		{
			super();
			addChild(_design);
			
			_design.bSound = Main.singletron._soundOn;
			_design.addEventListener(Event.ENTER_FRAME, checkLastFrame);
		}
		
		private function checkLastFrame(evt:Event):void
		{
			if( _design.currentFrame == _design.totalFrames)
			{
				_design.stop();
				_design.removeEventListener(Event.ENTER_FRAME, checkLastFrame);
				Main.singletron.screenManager.LoadScreen(new GameScreen());
			}
		}
		
		public function setUp(fr1Id:String, fr2Id:String):void
		{			
			
			//loadMeImage(fr1Id);
			//loadFriendImage(fr2Id);
		}

		
		
		private var url1Loader:URLLoader;
		private var url2Loader:URLLoader;
		private var url1Request:URLRequest;
		private var url2Request:URLRequest;
		private var image1Loader:Loader;
		private var image2Loader:Loader;
		
		/*
		public function loadMeImage(id:String ):void
		{			
			try
			{
				url1Request = new URLRequest("facebookProxy.php");
				var variables:URLVariables = new URLVariables();
				variables.path = "http://graph.facebook.com/" + id + "/picture?type=square";
				url1Request.data = variables;
				
				url1Loader = new URLLoader();
				url1Loader.dataFormat = URLLoaderDataFormat.BINARY;
				url1Loader.addEventListener(Event.COMPLETE, completeHandler);
				url1Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				url1Loader.load(url1Request);
			}catch(e:Error) 
			{
			}
		}
		
		private function completeHandler(event:Event):void
		{
			try
			{
				var byteArray:ByteArray = url1Loader.data;

				url1Loader.removeEventListener(Event.COMPLETE, completeHandler);
				
				image1Loader = new Loader();
				image1Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, friendImageLoaded);
				image1Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				image1Loader.loadBytes(byteArray);
			}catch(e:Error) 
			{
			}
		}
		
		private function meImageLoaded(e:Event):void
		{
			var image:Bitmap = new Bitmap(e.target.content.bitmapData);
			image.smoothing = true;
			image.width = 130;
			image.height = _imageLoader.height * 0.65;
			player2Mc.imageMc.removeChildAt(0)
			player2Mc.imageMc.addChild(image);
		}
		
		
		public function loadFriendImage( id:String ):void
		{
			try
			{
				url2Request = new URLRequest("facebookProxy.php");
				var variables:URLVariables = new URLVariables();
				variables.path = "http://graph.facebook.com/" + id + "/picture?type=square";
				url2Request.data = variables;
				
				url2Loader = new URLLoader();
				url2Loader.dataFormat = URLLoaderDataFormat.BINARY;
				url2Loader.addEventListener(Event.COMPLETE, completeHandlerFriend);
				url2Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				url2Loader.load(url2Request);
			}catch(e:Error) 
			{
			}
		}
		
		private function completeHandlerFriend(event:Event):void
		{
			try
			{
				var byteArray:ByteArray = url2Loader.data;

				url2Loader.removeEventListener(Event.COMPLETE, completeHandlerFriend);
				
				image2Loader = new Loader();
				image2Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, friendImageLoaded);
				image2Loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				image2Loader.loadBytes(byteArray);
			}catch(e:Error) 
			{
			}
		}
		
		private function friendImageLoaded(e:Event):void
		{
			var image:Bitmap = new Bitmap(e.target.content.bitmapData);
			image.smoothing = true;
			image.width = 130;
			image.height = image.height * 0.65;
			player2Mc.imageMc.removeChildAt(0)
			player2Mc.imageMc.addChild(image);
		}
		
		
		private function onIOError(evt:IOErrorEvent):void
		{
			//_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		
		*/
	}
}
