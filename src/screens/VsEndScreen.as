﻿package screens
{
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	
	import com.screenmanager.Screen;
	import goapi.GoApi;
	import goapi.apis.events.ScoreResult;
	import game.LanguageManager;
	
	
	public class VsEndScreen extends Screen
	{
		private var _design:VsEndScreen_design = new VsEndScreen_design();
		
		private var bSharedScore:Boolean;
		private var score:int;
		private var rank:uint;
		private var facebookName:String;
		
		private var _currLanguage:uint;
		
		public function VsEndScreen( _score:int, _wordsTot:int = 0, _wordsFound:int = 0, _scoreFrom:int = -1, _nameFrom:String = ""):void
		{
			super();
			addChild(_design);
			
			//_rankingRanges:String
			//_facebookName:String,
			// constructor code
			
			//_friendId = "0";
			//_requestId = "";
			//_nameFrom = "";
			//_scoreFrom = 0;
			//_permutationToUse = "";
			
			Main.singletron.setVolControlsVisible(true);
			
			
			//_design.btnShare.addEventListener(MouseEvent.CLICK, displayScoreOnFaceBook);  this handled in the class
			_design.btnExit.addEventListener(MouseEvent.CLICK, btnExit_click);
			_design.btnPlayAgain.addEventListener(MouseEvent.CLICK, btnPlayAgain_click );
			
			
			
			// TODO: get rank based on score
			rank = 1;
			
			/*
			var ranking:String;
		
			
			//if (_gameMode == _globalVars.GAME_MODE_CLASSIC || _gameMode == _globalVars.GAME_MODE_CLASSIC_VERSUS)
			ranking = _goApi.loaderVars.gameRankings10;
			
			if( _requestId == "")
				_scoreScreen.setup( _gameMode, _score, ranking, _goApi.loaderVars.facebookName, -1, "", _wordsTot, _wordsFound);
			else
				_scoreScreen.setup(_gameMode, _score, ranking, _goApi.loaderVars.facebookName, _scoreFrom, _nameFrom, _wordsTot, _wordsFound);
			*/
			
			
			// Select language
			_currLanguage = LanguageManager.getInstance().getLanguage();
			
			var sScoreTitle:String;
			
			//if( _gameMode == 0 || _gameMode == 10 )
				sScoreTitle = LanguageManager.getInstance().getTranslation("CLASSIC_MODE");
			
			
			_design.txtScoreTitle.text = LanguageManager.getInstance().getTranslation("SCORED") + ": (" + sScoreTitle + ")";
			_design.txtScore.text = _score.toString();
			score = _score;
			
			//TODO: facebookBtn.addEventListener(MouseEvent.CLICK, shareScore);
			_design.txtAgain.text = LanguageManager.getInstance().getTranslation("PLAY_AGAIN");
			_design.txtAgain.mouseEnabled = false;
			
			_design.mShareLang.gotoAndStop(_currLanguage + 1);
			_design.mShareLang.mouseEnabled = false;
			
			//facebookName = _facebookName;
			
			
			// TODO: Work out rank number
			/*
			var ranks:Array = new Array;
			var detail:Array = new Array;
			ranks = _rankingRanges.split(".");
			
			for each( var rankItem:String in ranks )
			{
				detail = rankItem.split("=");
				if( _score < detail[1])
					rank = detail[0];
			}
			trace("rank=" +  rank.toString());
			*/
			
			// Award highest rank achevement?
			if ( rank == 1)
			{
				GoApi.Instance.awardAchevementStep(6);
				// Unlock Word Invader game
				//TODO: GoApi.Instance.givePermission("canplay", 5);
			}
			
			// Update screen with outcomes
			if( _nameFrom != "")
			{
				// This was the second part of a challenge, show party A's score
				//_design.p2outcome.text = _nameFrom + " scored " + _scoreFrom;
				//if( _score > _scoreFrom)
				//	_design.p2outcome.appendText("\n"  + LanguageManager.getInstance().getTranslation("YOU_WON") );
				//else
				//	_design.p2outcome.appendText("\n"  + LanguageManager.getInstance().getTranslation("YOU_LOST"));
			}else
				//_design.p2outcome.text = LanguageManager.getInstance().getRankDesc(uint(rank));  // Single player, or 1st part of challange
			
			//TODO: _design.txtRank.text = LanguageManager.getInstance().getRankName(uint(rank));
			
			
			// Select language to show
			_design.gotoAndStop(_currLanguage + 1);
			
			
			// Save score // TODO: time elapsed
			Main.singletron.goApi.goScore.addEventListener(ScoreResult.SCORE_RESULT, goScore_result);
			Main.singletron.goApi.goScore.save(score, 0, "language:" + _currLanguage, "wordsTot:" + _wordsTot + ",wordsFound:" + _wordsFound + ",rank" + rank);
			
			
			// Load rank background
			loadRankScreen(rank.toString());
			
			bSharedScore = false;
		}
		
		private function goScore_result(e:ScoreResult):void 
		{
			trace("goScore_result = " + e._submitted);
		}
		
		private function btnExit_click(e:MouseEvent):void 
		{
			Main.singletron.screenManager.LoadScreen( new MenuScreen());
		}
		
		private function btnPlayAgain_click(e:MouseEvent):void 
		{
			Main.singletron.screenManager.LoadScreen( new InstructionsScreen());
		}
		
		
		private function loadRankScreen(sRank:String):void
		{
			var mLoader:Loader = new Loader();
			var mRequest:URLRequest = new URLRequest("rank_screens/rank" + sRank + ".swf");
			trace("sRank=" +  sRank);
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadRankScreen_complete);
			mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, loadRankScreen_progress);
			mLoader.load(mRequest);
			
			trace("loadRankScreen");
			
			// Show preloader
			//_design.mLoading.gotoAndStop(2);
		}

		private function loadRankScreen_complete(loadEvent:Event):void
		{
			var newScreen:MovieClip = MovieClip(loadEvent.currentTarget.content);
			_design.mRankBg.addChild(newScreen);
			
			//setChildIndex(_design._profileScreen, numChildren -1 );
			
			trace("loadRankScreen_complete");
			
			// Hide preloader
			//_design.mLoading.gotoAndStop(1);
		}
		
		private function loadRankScreen_progress(mProgress:ProgressEvent):void
		{
			var percent:Number = mProgress.bytesLoaded/mProgress.bytesTotal;
			trace(percent);
		}
		
		
		
		
		
		
		
		
		private function shareScore(evt:MouseEvent):void
		{
			
			var streamTitle:String = LanguageManager.getInstance().getScoreTitleFeed();
			var streamText:String = LanguageManager.getInstance().getScoreTextFeed(facebookName, score);
			var streamMsg:String = LanguageManager.getInstance().getTranslation("CAN_YOU_DO_BETTER");
			
			Main.singletron.goApi.goFacebook.streamScore(streamTitle, streamText, streamMsg, rank);
			
			// TODO: only award when stream is confirmed
			// Save achievement step
			if( !bSharedScore)
				Main.singletron.goApi.awardAchevementStep(1);
			
			bSharedScore = true;
		}
		
	}
}
