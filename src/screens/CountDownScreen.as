﻿package screens
{
	import flash.events.Event;
	import com.screenmanager.Screen;
	
	public class CountDownScreen extends Screen
	{
		private var design:CountDownScreen_design = new CountDownScreen_design;
		
		public function CountDownScreen()
		{
			super();
			addChild(design);
			design.bSound = Main.singletron._soundOn;
			design.addEventListener(Event.ENTER_FRAME, checkLastFrame);
		}
		
		private function checkLastFrame(evt:Event):void
		{
			//trace("checkLastFrame");
			if(design.currentFrame == design.totalFrames)
			{
				// Did permutations load?
				//if( Vocabularious(this.parent)._goApi.vocabPermLoaded)
				//{
				
					design.stop();
					design.removeEventListener(Event.ENTER_FRAME, checkLastFrame);
					Main.singletron.screenManager.LoadScreen(new GameScreen());
				//}else
				//{
					// Wait for permutations to load
					//gotoAndPlay(totalFrames - 10); 
				//}
			}
		}
	}
}
