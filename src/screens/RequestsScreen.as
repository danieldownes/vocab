﻿package screens
{
	import com.screenmanager.Screen;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.external.ExternalInterface;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.errors.IOError;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import goapi.GoApi;
	
	import caurina.transitions.Tweener;
	
	import screenparts.*;
	
	import events.RemoveRequest;
	
	import game.LanguageManager;

	public class RequestsScreen extends Screen
	{
		private var _design:RequestsScreen_design = new RequestsScreen_design();
		
		private const GAP:uint = 5;
		
		private var _meId:String;
		private var _stepHeight:Number = 54.6;
		private var _visibleFriends:uint = 7;
		
		//private var deletedReqsArr:Array = new Array;
		
		private var myLoader:URLLoader = new URLLoader();
		
		
		public function RequestsScreen()
		{
			super();
			addChild(_design);
			
			//trace("_goApi.loaderVars.gameRequests=" + _goApi.loaderVars.gameRequests);
			//trace("_goApi.loaderVars.gameResults=" + _goApi.loaderVars.gameResults);
		
			var req:RequestListMc;
			var requestsListMc:MovieClip = new MovieClip();
			var str1:String;
			var i:uint;
			var iOffset:uint = 1;
			
			// Add outstanding requests ..
			var a_me_requests:String = ""; // TODO: GOapi challanges
			if( a_me_requests != "")
			{
				var reqArr:Array = a_me_requests.split(".");
				for (i = 0; i < reqArr.length; i++)
				{
					req = new RequestListMc();
					str1 = reqArr[i];
					var eachReqArr:Array = str1.split("=");
					trace("add request:" + str1);
					req.setup(eachReqArr[0], eachReqArr[1], eachReqArr[2], eachReqArr[3], eachReqArr[4], eachReqArr[5]);
					req.y = (req.height + GAP) * (i+1);
					req.btnAccept.addEventListener(MouseEvent.CLICK, acceptRequest);
					req.btnIgnore.addEventListener(MouseEvent.CLICK, ignoreRequest);
					_design.requestsListMc.addChild(req);
				}
				iOffset = reqArr.length + 1;
			}
			
			
			// Add outstanding results
			var a_results:String = ""; //TODO: API Challange
			if ( a_results != "")
			{
				var resultsArr:Array = a_results.split(".");
				for ( i = 0; i < resultsArr.length; i++)
				{
					req = new RequestListMc();
					str1 = resultsArr[i];
					// $fId=$to_profileId=$name=$to_score=$from_score=$gameMode=$challengeId
					var eachArr:Array = str1.split("=");
					req.setupResult(eachArr[0], eachArr[1], eachArr[2], eachArr[3], eachArr[4], eachArr[5], eachArr[6]);
					req.y = (req.height + GAP) * (i + iOffset);
					req.btnRemove.addEventListener(MouseEvent.CLICK, closeResult);
					_design.requestsListMc.addChild(req);
				}
			}
			
			// Update scroller controls
			if( a_me_requests != "" || a_results != "")
			{
				_design.scrollerMc.containerMc.addChild(requestsListMc);
				_design.scrollerMc.containerMc.steps = 0;
				if( _design.requestsListMc.height <= _design.scrollerMc.maskMc.height)
				{
					_design.scrollerMc.upBtn.visible = false;
					_design.scrollerMc.downBtn.visible = false;
				}else
				{
					_design.scrollerMc.containerMc.totalSteps = _visibleFriends - (i + iOffset + 1);
					_design.scrollerMc.upBtn.alpha = .3;
					_design.scrollerMc.upBtn.addEventListener(MouseEvent.CLICK, moveUp)
					_design.scrollerMc.downBtn.addEventListener(MouseEvent.CLICK, moveDown)
				}
			}
			
			_design.btnClose.addEventListener(MouseEvent.CLICK, btnClose_click);
		}
		
		
		private function btnClose_click(e:MouseEvent):void 
		{
			Main.singletron.screenManager.LoadScreen(new MenuScreen());
		}
		
		private function removeRequest(evt:RemoveRequest):void
		{
			// TODO: GOAPI challange
			/*
			if( evt.requestType == "request")
				Main.singletron.goApi.goChallange.removeChallenge(evt.requestId);
			else if( evt.requestType == "result")
				Main.singletron.goApi.goChallange.markSeenResult(evt.requestId);
			*/
		}
		
		
		private function moveDown(evt:MouseEvent):void
		{
			if( _design.scrollerMc.containerMc.steps > _design.scrollerMc.containerMc.totalSteps)
			{
				_design.scrollerMc.containerMc.steps--;
				if( _design.scrollerMc.containerMc.steps == _design.scrollerMc.containerMc.totalSteps)
					_design.scrollerMc.downBtn.alpha = .3;
					
				_design.scrollerMc.upBtn.alpha = 1;
				Tweener.addTween(_design.scrollerMc.containerMc,{y:_design.scrollerMc.containerMc.steps * _stepHeight,time:.6});
			}
		}
		private function moveUp(evt:MouseEvent):void
		{
			if( _design.scrollerMc.containerMc.steps < 0)
			{
				_design.scrollerMc.containerMc.steps++;
				if( _design.scrollerMc.containerMc.steps == 0)
					_design.scrollerMc.upBtn.alpha = .3;
				
				_design.scrollerMc.downBtn.alpha = 1;
				Tweener.addTween(_design.scrollerMc.containerMc,{y:_design.scrollerMc.containerMc.steps * _stepHeight,time:.6});
			}
		}
		
		private function acceptRequest(evt:MouseEvent):void
		{
			var req:RequestListMc = evt.currentTarget.parent as RequestListMc;
			
			// Start the game
			trace( "req.gameMode=" + req.gameMode);
			//this.dispatchEvent(new StartMultipleGame(req.id, req.reqId, req.fullName, req.from_score, req.gameMode, req.permutation, "requests", true));
		}
		
		
		private function ignoreRequest(evt:MouseEvent):void
		{
			var req:RequestListMc = evt.currentTarget.parent as RequestListMc;
			
			// Mark this as removed
			//deletedReqsArr[deletedReqsArr.length] = req.reqId;
			
			// Send feedback to Facebook
			this.dispatchEvent(new RemoveRequest( req.reqId, "request", true));
			
			// Fade the list item
			Tweener.addTween(req,{alpha:0.3,time:.6});
			//req.alpha = .3;
			req.btnAccept.visible = false;
			req.btnIgnore.visible = false;
			req.nameTxt.appendText("  ("+ LanguageManager.getInstance().getTranslation("IGNORED") +")");
		}
		
		private function closeResult(evt:MouseEvent):void
		{
			var req:RequestListMc = evt.currentTarget.parent as RequestListMc;
			
			// Update DB
			this.dispatchEvent(new RemoveRequest( req.reqId, "result", true));
			
			// Fade the list item
			Tweener.addTween(req,{alpha:0.3,time:.6});
			//req.alpha = .3;
			req.btnRemove.visible = false;
			req.nameTxt.appendText("  ("+ LanguageManager.getInstance().getTranslation("REMOVED") +")");
		}
		
	}
}
