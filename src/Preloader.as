package
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.utils.getDefinitionByName;
	import flash.utils.Timer;
		
	
	[SWF(width="700",height="510",frameRate="30",backgroundColor="0x330066",quality="high",scale="noscale")]
	public class Preloader extends MovieClip
	{
		private var maxWidth:Number;
		public var preloader_mc:vocabularious_preloader= new vocabularious_preloader();
		private var exitTimer:Timer = new Timer(1700, 0);
		
		public function Preloader()
		{
			if (stage)
				stage.scaleMode = StageScaleMode.NO_SCALE;
			
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			//loaderInfo.addEventListener(ProgressEvent.COMPLETE, progressComplete);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);

			//maxWidth = preloader_mc.preloaderBar_mc.width;
			//preloader_mc.txtPercent.text = "0%";
			//preloader_mc.gotoAndStop(1);
			
			addChild(preloader_mc); //add the preloader to stage
			
			// Need room for Go Bar?
			if( "platform" == "not_portal")
			{
				preloader_mc.x = 65;
				preloader_mc.y = 45;
			}
		}
		
		private function progressComplete(e:ProgressEvent):void
		{
			trace("progressComplete");
			//preloader_mc.gotoAndStop(100);
		}
		
		private function progress(e:ProgressEvent):void
		{
			var percent:int = Math.round(e.bytesLoaded / e.bytesTotal * 100);
			
			//preloader_mc.preloaderBar_mc.width = percent / 100 * maxWidth;
			//preloader_mc.txtPercent.text = percent.toString() + " %";
			//trace( "per:" + percent);
			if( percent > 0 && percent <= 100)
				preloader_mc.gotoAndStop(percent);
			
			//trace("progress=" + percent);
		}
		
		private function ioError(e:IOErrorEvent):void
		{
			trace(e.text);
		}
		
		private function checkFrame(e:Event):void
		{
			if(currentFrame == totalFrames)
			{
				//stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			startup();
			exitTimer.addEventListener(TimerEvent.TIMER, unloadMe);
			exitTimer.start();
		}
		
		private function unloadMe(e:Event):void
		{
			removeChild(preloader_mc);
			exitTimer.removeEventListener(TimerEvent.TIMER, unloadMe);
		}
		
		private function startup():void
		{
			//trace(framesLoaded);
			var mainClass:Class = getDefinitionByName("Main") as Class; //add the main class
			addChild(new mainClass() as DisplayObject);
		}
	}
}

