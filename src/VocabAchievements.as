package  
{
	import goapi.apis.datatypes.AchievementRequest;
	import goapi.apis.datatypes.GoApiAchievementType;
	import goapi.apis.GoApiAchievement;
	import goapi.apis.GoApiScore;
	import goapi.GoApi;
	import screenpopups.AchievementPopup;
	import screens.ScoreScreen;
	
	public class VocabAchievements 
	{
		private var passed:Array = new Array;
		private var seen:Array = new Array;
		
		// NOTE: If adding more achievemnts please increment
		public static const TOTAL:int = 20;
		
		public static const LETTERS_4:int = 36;		// Find your first 4 letter word
		public static const LETTERS_5:int = 37;		// Find your first 5 letter word
		public static const LETTERS_6:int = 38;		// Find your first 6 letter word
		public static const LETTERS_7:int = 39;		// Find your first 7 letter word
		public static const RANK_12:int   = 40;		// Rank 12
		public static const RANK_11:int   = 41;		// Rank 11
		public static const RANK_10:int   = 42;		// Rank 10
		public static const RANK_9:int	  = 43;		// Rank 9
		public static const RANK_8:int    = 44;		// Rank 8
		public static const RANK_7:int    = 45;		// Rank 7
		public static const RANK_6:int    = 46;		// Rank 6
		public static const RANK_5:int    = 47;		// Rank 5
		public static const RANK_4:int    = 48;		// Rank 4
		public static const RANK_3:int    = 49;		// Rank 3
		public static const RANK_2:int    = 50;		// Rank 2
		public static const RANK_1:int    = 51;		// Rank 1
		public static const ALL_RANKS:int = 52;		// Reach Every Rank
		public static const SHARER:int    = 53;		// Score Sharer
		public static const CHALLENGER:int = 54;	// Challenger
		public static const DEFEATER:int   = 55;	// Defeater
		
		public var arrayFoundWordLength:Array = new Array();	// Enable detection of 'Find your first x letter word'
		
		
		public function check():void
		{
			trace("check()");
			var idAndCount:Array = check_ok();
			if( idAndCount != null)
			{
				Main.singletron.screenManager.LoadScreen(new AchievementPopup(idAndCount[0]));
				awardAchievement(returnIncomepleteAchievement(idAndCount[0]), idAndCount[1]);
				seen[idAndCount[0]] = true;
			}else
			{
				/*
				 * TODO: Challenges
				trace("_requestId=" + Main.singletron._requestId);
				trace("_wager=" + Main.singletron._wager);
				
				// Challenge match-end OR single player roundup screen?
				if( Main.singletron._wager != "")
					Main.singletron.screenManager.LoadScreen(new VsEndScreen());
				else*/
					Main.singletron.screenManager.LoadScreen(new ScoreScreen());
			}
		}
		
		private function check_ok():Array
		{
			var ach:int;
			
			for( ach = LETTERS_4; ach <= LETTERS_7; ach++)
			{
				if( !alreadyAwarded(ach) && arrayFoundWordLength[ach - LETTERS_4])
					return new Array(ach, 1);
			}
			
			// Check rank gained
			var rank:int = GoApiScore.Instance.getRank(Main.singletron.score);
			var rankAchId:int = RANK_12 + (12 - rank);
			if( !alreadyAwarded(rankAchId))
				return new Array(rankAchId, 1);
			
			// Check if all ranks have been completed
			var achievements:Vector.<GoApiAchievementType> = GoApiAchievement.Instance.incompleteAchievements;
			var bAllRanks:Boolean = true;
			for( var r:int = 0; r < achievements.length && bAllRanks; r++)
			{
				if( achievements[r].achId >= RANK_12 && achievements[r].achId <= RANK_1 && achievements[r].achId != rankAchId)
					bAllRanks = false;
			}
			if( bAllRanks)
				return new Array(ALL_RANKS, 1);
			
			//TODO: Check defeater
			
			return null;
		}
		
		public function alreadyAwarded(achievementId:int):Boolean
		{
			// If already seen (in this session), then must have already been awarded
			if( seen[achievementId])
				return true;
			
			// Else check arrays
			var out:Boolean = false;
			var awarded:Vector.<GoApiAchievementType> = GoApiAchievement.Instance.awardedAchievements;
			
			for( var n:int = 0; n < awarded.length; n++)
			{
				if( awarded[n].achId == achievementId && awarded[n].check)
					return true;
			}
			
			return false;
		}
		
		public function resetPassed():void
		{
			passed.splice();
		}
		
		public function setPassed(id:int, steps:int = 1):void
		{
			//if( id==21)
			//	trace("SET PASSED:" + id + " steps:" + steps);
			passed[id] = steps;
		}
		
		public function returnIncomepleteAchievement(achievementId:int):GoApiAchievementType
		{
			var incompletes:Vector.<GoApiAchievementType> = GoApiAchievement.Instance.incompleteAchievements;
			
			for( var n:int = 0; n < incompletes.length; n++)
			{
				if( incompletes[n].achId == achievementId)
					return incompletes[n];
			}
			
			return null;
		}
		
		public function awardAchievement(achievementObj:GoApiAchievementType, steps:int = 1):void
		{
			// Save Achievement Steps
			var achReqObj:AchievementRequest = new AchievementRequest();
			achReqObj.achievementId = achievementObj.achId;
			achReqObj.profileId = GoApi.Instance.apiBase.profileId;
			achReqObj.steps = steps;
			achReqObj.recId = achievementObj.recId;
			trace("save achId:" + achievementObj.achId + " steps:" + steps + " recId:" + achievementObj.recId);
			GoApiAchievement.Instance.requestData = achReqObj;
			GoApiAchievement.Instance.award();
		}
	}

}