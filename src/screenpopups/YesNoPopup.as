package screenpopups 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.screenmanager.Screen;
	
	public class YesNoPopup extends Screen
	{
		public var _design:YesNoPopup_design = new YesNoPopup_design();	
		
		public function YesNoPopup(sMessage:String, bShowYes:Boolean = true, bShowNo:Boolean = true, sCloseOnEvent:String = "")
		{
			super();
			isPopup = true;
			this.addChild(_design);
			
			if( bShowYes)
				_design.btnYes.addEventListener(MouseEvent.CLICK, btnYes_click);
			else
				_design.btnYes.visible = false;
			
			if(bShowNo)
				_design.btnNo.addEventListener(MouseEvent.CLICK, btnNo_click);
			else
				_design.btnNo.visible = false;
			
			//if( sCloseOnEvent != "")
			//	Main.singletron.SFSConn.addEventListener(sCloseOnEvent, closeWaitingPopup);
			
			_design.tText.text = sMessage;
		}
		
		private function btnNo_click(eve:MouseEvent):void
		{
			ScreenExit();
		}
		
		private function btnYes_click(eve:MouseEvent):void
		{
			ScreenExit();
		}
		
		/*
		private function closeWaitingPopup(e:Event):void 
		{
			ScreenExit();
		}
		*/
		
	}

}