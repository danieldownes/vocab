package screenpopups
{
	import com.screenmanager.Screen;
	import events.SelectLanguage;
	
	import game.*;
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	
	public class LanguageScreenPopup extends Screen
	{
		private var design:LanguageScreenPopup_design = new LanguageScreenPopup_design;
		
		private const LANGGAP:uint = 3;
		
		public function LanguageScreenPopup()
		{
			super();
			addChild(design);
			isPopup = true;
			/*
			for( var i:int = 0; i < DictionaryManager.getInstance().getDictionaryList().length; i++)
			{
				var newLangMc:LangMc = new LangMc();
				newLangMc.id = i;
				newLangMc.txt.text = DictionaryManager.getInstance().getDictionaryList()[i]._name;
				newLangMc._code = DictionaryManager.getInstance().getDictionaryList()[i]._code;
				newLangMc._id = DictionaryManager.getInstance().getDictionaryList()[i]._id;
				newLangMc.x = LANGGAP;
				newLangMc.y = i * (newLangMc.height + LANGGAP) + LANGGAP * 1.5
				newLangMc.buttonMode = true;
				newLangMc.mouseChildren = false;
				newLangMc.addEventListener(MouseEvent.CLICK, newDictionaryClicked)
				design.allLangMc.addChild(newLangMc);
			}
			
			design.bgMc.addEventListener(MouseEvent.CLICK, bgMc_click)
			*/
		}
		
		private function newDictionaryClicked(evt:MouseEvent):void
		{
			var mc:MovieClip = evt.currentTarget as MovieClip;
			Main.singletron.gameApi.vocabDicId = mc._id;
			
			trace("mc._id=" + mc._id);
			var _selectedLang:int;
			
			// Also change language option
			if( mc._id == 7 || mc._id == 8)
				_selectedLang = 0;
			else if ( mc._id == 9)
				_selectedLang = 2;
			else if( mc._id == 10)
				_selectedLang = 1;
			else if( mc._id == 11)
				_selectedLang = 4;
			else if( mc._id == 12)
				_selectedLang = 3;
			else if( mc._id == 13)
				_selectedLang = 5;
			
			Main.singletron.dispatchEvent(new SelectLanguage(_selectedLang));
			Main.singletron.PlaySound(new snd_selLang);
			
			Exit();
		}
		
		private function bgMc_click(e:MouseEvent):void 
		{
			Exit();
		}
		
	}

}