package screenpopups
{
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.events.Event;

	import com.screenmanager.Screen;
	import goapi.GoApi;
	
	public class SignupPopup extends Screen
	{
		private var design:GoSignup_design = new GoSignup_design();
		
		private var bDontShow:Boolean = false;
		
		public function SignupPopup()
		{
			super();
			addChild(design);
			isPopup = true;
			
			design.btnSignup.addEventListener(MouseEvent.CLICK, btnSignup_click);
			design.btnClose.addEventListener(MouseEvent.CLICK, btnClose_click);
		}
		
		private function btnSignup_click(e:MouseEvent):void 
		{
			navigateToURL(new URLRequest(GoApi.Instance.BASE_URL + "login"));
		}
		
		private function btnClose_click(e:MouseEvent):void 
		{
			ScreenExit();
		}
	}
}