package screenpopups 
{
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.screenmanager.Screen;
	
	import goapi.GoApi;
	
	public class AchievementPopup extends Screen
	{
		public var _design:AchievementsScreen_design = new AchievementsScreen_design();	
		
		public function AchievementPopup(achievementId:int) 
		{
			super();
			this.addChild(_design);
			
			trace("achievementId = " + achievementId);
			
			// Select Achievement (35 = offset of achievement IDs in database)
			_design.trophy_holder.gotoAndStop(achievementId - 35);
			
			// Relivant text
			if( achievementId == VocabAchievements.LETTERS_4)
			{
				_design.tName.text = "First 4 Letter Word!";
				_design.tReason.text = "";
			}else if( achievementId == VocabAchievements.LETTERS_5)
			{
				_design.tName.text = "First 5 Letter Word!";
				_design.tReason.text = "";
			}else if( achievementId == VocabAchievements.LETTERS_6)
			{
				_design.tName.text = "First 6 Letter Word!";
				_design.tReason.text = "";
			}else if( achievementId == VocabAchievements.LETTERS_7)
			{
				_design.tName.text = "First 7 Letter Word!";
				_design.tReason.text = "";
				
			}else if( achievementId >= VocabAchievements.RANK_12 && achievementId <= VocabAchievements.RANK_1 )
			{
				var iRank:int = (12 - (achievementId - VocabAchievements.RANK_12));
				_design.tName.text = "Rank " + iRank.toString();
				_design.tReason.text = "First time to achieve a score that is within the range of rank " + iRank.toString();
			}else if( achievementId == VocabAchievements.ALL_RANKS)
			{
				_design.tName.text = "Reach Every Rank";
				_design.tReason.text = "Gain all 12 rank trophies";
			}else if( achievementId == VocabAchievements.SHARER)
			{
				_design.tName.text = "Score Sharer";
				_design.tReason.text = "Share your score 10 times on Facebook";
			}else if( achievementId == VocabAchievements.CHALLENGER)
			{
				_design.tName.text = "Challenger";
				_design.tReason.text = "Invite 10 Friends to a challenge";
			}else if( achievementId == VocabAchievements.DEFEATER)
			{
				_design.tName.text = "Defeater";
				_design.tReason.text = "Win 5 Challenge Matches";
			}
			
			Main._STAGE.addEventListener(KeyboardEvent.KEY_DOWN, next);
			
			//TODO: Facebook share achievment
			_design.btnShare.alpha = 0.3;
			_design.btnShare.mouseEnabled = false;
			//_design.btnShare.addEventListener(MouseEvent.CLICK, btnShare_click);
			_design.btnNext.addEventListener(MouseEvent.CLICK, btnNext_click);
			
		}
		
		private function btnShare_click(e:MouseEvent):void 
		{
			// TODO: Share stream
		}
		
		private function btnNext_click(e:MouseEvent):void 
		{
			next();
		}
		
		private function next(e:KeyboardEvent = null):void
		{
			Main._STAGE.removeEventListener(KeyboardEvent.KEY_DOWN, next);
			Main.singletron.PlaySound(new MenuClickSnd);
			
			Main.achievements.check();
		}
		
	}

}