package
{
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	import flash.system.Security;
	import flashx.textLayout.elements.GlobalSettings;
	
	import com.screenmanager.*;
	
	import goapi.*;
	import goapi.apis.*;
	import helpers.*;;
	
	import game.Global;
	import util.ArrayList;
	import screens.*;
	import events.*;
	
	
	[Frame(factoryClass="Preloader")]
	public class Main extends MovieClip
	{
		// Facebook size (with go bar, and friends): 760x820
		// Portal size (without go bar, and without friends): 695x510
		public static const GAME_ID:int =  1;
		public var GAME_ASSETS:String;
		
		public static var singletron:Main;
		public static var _STAGE:Stage;
		
		public static var achievements:VocabAchievements;
		
		public var screenManager:ScreenManager;
		
		public var _GoHolderScreen:GoHolder;
		public var _facebookScreen:FriendsBar;
		
		public var _bg:MovieClip;
		
		public var gameApi:Global;
		
		
		public var _gameMusic:Sound;
		public var _musicChannel:SoundChannel;
		public const MUSIC_VOL:Number = 0.4;
		public var soundOffBtn:SoundOffBtn;
		public var soundOnBtn:SoundOnBtn;
		public var musicOnOffBtn:MovieClip;
		public var bMusicPlaying:Boolean;
		public var _soundOn:Boolean;
		
		public var score:int = 0;
		public var wordsTot:int;
		public var wordsFound:int;		
		
		public var _friendId:String = "0";
		public var _requestId:String = "";
		public var _nameFrom:String = "";
		public var _scoreFrom:int = 0;
		public var _permutationToUse:String = "";
		
		public var selectedLang:uint = 0;
		
		public function Main():void
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_STAGE = stage;
			
			achievements = new VocabAchievements();
			
			// entry point
			singletron = this;
			
			// GO API
			GoApi.Instance.init(root.loaderInfo.parameters, GAME_ID);
			GoApi.Instance.goAchievement = GoApiAchievement.Instance;
			//GoApi.Instance.goChallenge = GoApiChallenge.Instance;
			GoApi.Instance.goScore = GoApiScore.Instance;
			
			GAME_ASSETS = GoApi.Instance.BASE_URL + "assets/games/vocab/";
			
			// GAME API
			gameApi = new Global();
			
			//Security.loadPolicyFile("http://profile.ak.fbcdn.net/crossdomain.xml");
			
			// TODO: GOAPI Preferences
			_soundOn = true;
			
			// TODO: Consider moving outside of Main.as
			_bg = new MovieClip();
			addChild(_bg);
			_bg.addChildAt( new bgMc() , 0);
			
			soundOffBtn = new SoundOffBtn();
			soundOffBtn.x = 69.9;
			soundOffBtn.y = 10;
			_bg.addChild(soundOffBtn);
			soundOffBtn.addEventListener(MouseEvent.CLICK, soundOffBtnClicked);
			
			soundOnBtn = new SoundOnBtn();
			soundOnBtn.x = 20;
			soundOnBtn.y = 10;
			soundOnBtn.alpha = .25;
			soundOnBtn.mouseEnabled = false;
			_bg.addChild(soundOnBtn);
			soundOnBtn.addEventListener(MouseEvent.CLICK, soundOnBtnClicked);
			
			musicOnOffBtn = new MusicOnOffBtn()
			musicOnOffBtn.x = 120;
			musicOnOffBtn.y = 10;
			musicOnOffBtn.buttonMode = true;
			musicOnOffBtn.useHandCursor = true;
			_bg.addChild(musicOnOffBtn);
			musicOnOffBtn.addEventListener(MouseEvent.CLICK, soundBtnMusic);
			
			_gameMusic = new Sound( new URLRequest(Main.singletron.GAME_ASSETS + "music.mp3"));
			_musicChannel = _gameMusic.play(0, int.MAX_VALUE);
			_musicChannel.soundTransform = new SoundTransform(MUSIC_VOL);
			
			bMusicPlaying = false; // TODO: Goapi
			if( !bMusicPlaying)
				soundBtnMusic();
			
			// Go Holder should be loaded when not on portal
			if ( "platform" == "not_portal")
			{
				// GO Holder // TODO: Considor seperating into SWF and loading from GoAPI
				_GoHolderScreen = new GoHolder();
				addChild(_GoHolderScreen);
				setChildIndex(_GoHolderScreen, numChildren -1 );
				
				// Move game holder
				_bg.x = 65;
				_bg.y = 45;
			}
			
			
			// Assign a new screenmanager and load a new screen.
			this.addEventListener(Event.ENTER_FRAME, UpdateFrame);
			screenManager = new ScreenManager(_bg, 0);
			
			// Are there any open challenge requests?
			//trace("requests:" + _goApi.loaderVars.gameRequests);
			//if ( _goApi.loaderVars.gameRequests != "" || _goApi.loaderVars.gameResults != "" && _goApi.loaderVars.gameRequests != null)
			//	Main.singletron.screenManager.LoadScreen(new RequestsScreen());
			//else
				screenManager.LoadScreen(new MenuScreen());
			
			addEventListener(SelectLanguage.SELECT_LANGUAGE, onSelectLanguage);
			
			// Setup fb area
			_facebookScreen = new FriendsBar();
			_facebookScreen.x = 65;
			_facebookScreen.y = 552;
			//addChild(_facebookScreen);
		}
		
		private function UpdateFrame(e:Event):void
		{
			// Update our screen manager every frame.
			screenManager.Update();
		}
		
		public function jsMsgSaveInvites(ids:Array):void
		{
			//deepTrace(ids);
			trace("jsMsgSaveInvites");
			//gameApi.saveInvitedFacebookIds(ids);
		}
		
		
		public function setGameMode(gameMode:int):void
		{
			// Ensure challenge flags are not set at this point
			_friendId = "0";
			_requestId = "";
			_nameFrom = "";
			_scoreFrom = 0;
			_permutationToUse = "";
			//_modeMenuScreen.setup(LanguageManager.getInstance().getLanguage(),_globalVars,_gameMode);
		}
		
		
		// --------------------- EventHandlers ------------------------ //
		
		/*
		// Ensure buttons stay on top
		setChildIndex( musicOnOffBtn, this.numChildren -1 );
		setChildIndex( soundOnBtn, this.numChildren -1 );
		setChildIndex( soundOffBtn, this.numChildren -1 );
		*/
			
		private function onSelectLanguage(evt:SelectLanguage):void
		{
			selectedLang = evt._languageId;
		}
		
		private function soundOnBtnClicked(evt:MouseEvent = null):void
		{
			_soundOn = true
			
			soundOnBtn.alpha = 0.25;
			soundOnBtn.mouseEnabled = false;
			
			soundOffBtn.alpha = 1;
			soundOffBtn.mouseEnabled = true;
		}
		
		private function soundOffBtnClicked(evt:MouseEvent = null):void
		{
			_soundOn = false
			
			soundOnBtn.alpha = 1;
			soundOnBtn.mouseEnabled = true;
			
			soundOffBtn.alpha = 0.25;
			soundOffBtn.mouseEnabled = false;
		}
		
		private function soundBtnMusic(evt:MouseEvent = null):void
		{
			if( bMusicPlaying)
			{
				_musicChannel.soundTransform = new SoundTransform(0);
				musicOnOffBtn.mIcon.gotoAndStop(2); // No music
			}else
			{
				_musicChannel.soundTransform = new SoundTransform(MUSIC_VOL);
				musicOnOffBtn.mIcon.gotoAndStop(1); // Music playing
			}
			bMusicPlaying = !bMusicPlaying;
		}
		
		public function setVolControlsVisible(bVisible:Boolean):void
		{
			soundOffBtn.visible = bVisible;
			soundOnBtn.visible = bVisible;
			musicOnOffBtn.visible = bVisible;
		}
		
		public function PlaySound(sound:Sound):void
		{
			if( _soundOn)
				sound.play();
		}
		
		
		public static function get STAGE():Stage
		{
			return _STAGE;
		}
	}
}