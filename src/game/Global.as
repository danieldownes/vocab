﻿package game
{
	import flash.net.NetConnection;
	import flash.net.Responder;
	import goapi.GoApi;
	
	public class Global
	{
		// Game Modes
		public static const GAME_MODE_CLASSIC:int				= 0;
		public static const GAME_MODE_CLASSIC_VERSUS:int  		= 10;
		
		
		// List of External Swfs
		public static const PROFILE_SWF:int						= 0;
		public static const MATES_SWF:int						= 1;
		public static const SHOP_SWF:int						= 2;
		
		
		public var vocabDicId:int;
		public var vocabWord:String = "CEHRSSU";
		public var vocabPermutations:String = "CEHU,CEHSU,CEHRSSU";
		
		
		public function Global():void
		{
			vocabDicId = 7;
		}
		
		public function loadPermutation(dicId_:int, gameMode_:int, permutation_:String):void
		{
			var permType:VocabLoadPermType = new VocabLoadPermType();
			permType.dicId = vocabDicId;
			permType.gameMode = gameMode_; // 0,2 or 3
			permType.permutation = permutation_;
			trace("loadPermutation");
			GoApi.Instance.apiBase.net.call("Vocab_service/getPerms", new Responder(onResult, onFault), permType);
		}
		
		private function onResult(result:Object):void
		{
			//var _message:String = result.toString();
			
			//trace("_message:" + _message);
			
			//vocabWord = 
			//if(result.result == "ok")
			vocabPermutations =  String(result.words);
			vocabWord =  String(result.perm);
			
			trace("vocabPermutations:" + vocabPermutations);
			trace("vocabWord:" + vocabWord);
		}
		
		private function onFault(event:Object):void
		{
			var _message:String = "onFault: " + String(event);
			trace(_message);
		}
		
	}
}