package game 
{
	import util.ArrayList;
	import flash.utils.ByteArray;
	
	public class Dictionary 
	{
		public var _name:String;
		public var _code:String;
		public var _id:int;
		
		public var _words:Array;
		
		public function Dictionary( name:String, code:String, data:ByteArray, id:int ) 
		{
			_name = name;
			_code = code;
			_id = id;
		}
	}
}