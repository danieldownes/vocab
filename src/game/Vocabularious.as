﻿package
{
	import util.SWFBridgeAS3;
	
	import GoApi;
	
	//import org.as3commons.logging.*;
	//import org.as3commons.logging.getLogger;
	
	import caurina.transitions.Tweener;
	
	import screens.*;
	import events.*;
	import Global;
	import core.LanguageManager;
	import core.DictionaryManager;
	
	
	public class Vocabularious extends Sprite
	{
		public var vocabPermLoaded:Boolean = false;
		
		private var swfBridge:SWFBridgeAS3 = new SWFBridgeAS3("go_vocab", this);
		private var iSwfScreen:int;
		
		public function loadExternalSwf(e:LoadSwf):void
		{
			trace("loadExternal=" + e.ind);
			
			if( e.ind == _globalVars.PROFILE_SWF || e.ind == _globalVars.MATES_SWF)
			{
				iSwfScreen = e.ind;
				
				if ( !bProfileLoaded)
				{
					loadProfileScreen();
					
				}else
				{
					_profileScreen.visible = true;
					swfBridge.send("changeScreen", iSwfScreen);
					setChildIndex(_shopScreen, this.numChildren -1 );
				}
			}
			
			if( e.ind == _globalVars.SHOP_SWF)
			{
				if ( !bShopLoaded)
				{
					loadShopScreen();
				}else
				{
					_shopScreen.visible = true;
					setChildIndex(_shopScreen, this.numChildren -1 );
				}
			}
		}
		
		
		private var mLoader:Loader;
		
		private function loadProfileScreen():void
		{
			var AD:ApplicationDomain = new ApplicationDomain( null );
			var context:LoaderContext = new LoaderContext( false, AD );
			var mRequest:URLRequest = new URLRequest("../GoProfile_r" + _goApi.loaderVars.releaseNum +".swf");
			mLoader = null;
			mLoader = new Loader();
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadProfileScreen_complete);
			//mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, loadProfileScreen_progress);
			mLoader.load(mRequest, context);
		}

		private function loadProfileScreen_complete(loadEvent:Event):void
		{
			_profileScreen = MovieClip(loadEvent.currentTarget.content);
			_profileScreen.addEventListener(StartMultipleGame.START_MULTIPLE_GAME, displayMultipleInstructionsScreen);
			addChild(_profileScreen );
			setChildIndex(_profileScreen, numChildren -1 );
			
			trace("loadProfileScreen_complete");
			bProfileLoaded = true;
			
			// Send friend info
			swfBridge.addEventListener(Event.CONNECT,onConnect);
		}
		
		private function onConnect(p_evt:Event):void
		{
			trace("onConnect:" + p_evt);
			swfBridge.send("changeScreen", iSwfScreen);
			
			
			swfBridge.send("sendUserId", _goApi.loaderVars.goUserId);
			//swfBridge.send("updateFriendList", _goApi.loaderVars.goVocabFriends);
		}
		/*
		private function loadProfileScreen_progress(mProgress:ProgressEvent):void
		{
			var percent:Number = mProgress.bytesLoaded/mProgress.bytesTotal;
			//trace(percent);
		}
		*/
		
		
		private function loadShopScreen():void
		{
			var AD:ApplicationDomain = new ApplicationDomain( null );
			var context:LoaderContext = new LoaderContext( false, AD );
			var mRequest:URLRequest = new URLRequest("../GoShop_r" + _goApi.loaderVars.releaseNum + ".swf");
			
			mLoader = null;
			mLoader = new Loader();
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadShopScreen_complete);
			mLoader.load(mRequest, context);
			
			
			trace("loadShopScreen");
			
			// Show preloader
			//_menuScreen.mLoadingShop.gotoAndStop(2);
		}

		private function loadShopScreen_complete(loadEvent:Event):void
		{
			_shopScreen = MovieClip(loadEvent.currentTarget.content);
			addChild(_shopScreen);
			setChildIndex(_shopScreen, numChildren -1 );
			
			trace("loadShopScreen_complete");
			
			bShopLoaded = true;
			
			// Hide preloader
			//_menuScreen.mLoadingShop.gotoAndStop(1);
		}
	}
}