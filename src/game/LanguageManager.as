﻿package game
{
	public class LanguageManager
	{
		// Languages
		public static const LANGUAGE_ENGLISH:uint = 0;
		public static const LANGUAGE_FRENCH:uint = 1;
		public static const LANGUAGE_SPANISH:uint = 2;
		public static const LANGUAGE_GERMAN:uint = 3;
		public static const LANGUAGE_ITALIAN:uint = 4;
		public static const LANGUAGE_PORTUGUESE:uint = 5;
		
		// Reference's
		public static const REFERENCE_INSTRUCTIONS_CLASSIC:uint = 0;
		public static const REFERENCE_INSTRUCTIONS_LEADERBOARD:uint = 1;
		public static const REFERENCE_INSTRUCTIONS_SPEEDOFLIGHT:uint = 2;
		public static const REFERENCE_INSTRUCTIONS_THEGRID:uint = 3;
		
		// Selected Language
		public var languageArr:Array;
		private var _selectedLanguage:uint = LANGUAGE_ENGLISH;
		
		// Look up arrays
		private var _translationsArr:Array = new Array();
		
		private var _instructionsLangArr:Array = new Array();
		private var _rankingsLangArr:Array = new Array();
		private var _rankDescLangArr:Array = new Array();
		
		private var _feedScoreTitleArr:Array = new Array();
		private var _feedScoreTextArr:Array = new Array();
		
		private var _feedChallengeTitleArr:Array = new Array();
		private var _feedChallengeArr:Array = new Array();
		
		private var _feedInviteArr:Array = new Array();
		
		// Singleton
		private static var instance:LanguageManager = new LanguageManager();
		
		public function LanguageManager()
		{
			if (instance)
				throw new Error("We cannot create a new instance.");
			else
				init();
		}
		
		public static function getInstance():LanguageManager
		{
			return instance;
		}
		
		public function init():void
		{
			// Languages Array
			languageArr = new Array("ENGLISH", "FRENCH", "SPANISH", "GERMAN", "ITALIAN", "PORTUGUESE");
			
			var transArrEnglish:Array = new Array();
			var transArrFrench:Array = new Array();
			var transArrSpanish:Array = new Array();
			var transArrGerman:Array = new Array();
			var transArrItalian:Array = new Array();
			var transArrPortuguese:Array = new Array();
			
			transArrEnglish['GAME_LANGUAGE'] = "Game Language";
			transArrFrench['GAME_LANGUAGE'] = "Langue du jeu";
			transArrSpanish['GAME_LANGUAGE'] = "Juego de lenguaje";
			transArrGerman['GAME_LANGUAGE'] = "Spielsprache";
			transArrItalian['GAME_LANGUAGE'] = "Gioco linguistico";
			transArrPortuguese['GAME_LANGUAGE'] = "Língua do jogo";
			
			
			transArrEnglish['SINGLE_PLAYER'] = "SINGLE PLAYER";
			transArrFrench['SINGLE_PLAYER'] = "Jouez en solo";
			transArrSpanish['SINGLE_PLAYER'] = "UN JUGADOR";
			transArrGerman['SINGLE_PLAYER'] = "Play Solo";
			transArrItalian['SINGLE_PLAYER'] = "Gioca da solo";
			transArrPortuguese['SINGLE_PLAYER'] = "jogar Solo";
			
			transArrEnglish['VERSUS_MODE'] = "VERSUS MODE";
			transArrFrench['VERSUS_MODE'] = "Mode de versets";
			transArrSpanish['VERSUS_MODE'] = "MODO VERSUS";
			transArrGerman['VERSUS_MODE'] = "Verse-Modus";
			transArrItalian['VERSUS_MODE'] = "Versus Mode";
			transArrPortuguese['VERSUS_MODE'] = "MODO VERSUS";
			
			
			transArrEnglish['WINS'] = "Wins";			transArrEnglish['WIN'] = "Win";
			transArrEnglish['LOSSES'] = "Losses";		transArrEnglish['LOSS'] = "Loss";
			transArrFrench['WINS'] = "Victoires";		transArrFrench['WIN'] = "GAGNER";
			transArrFrench['LOSSES'] = "défaites";		transArrFrench['LOSS'] = "PERTE";
			transArrSpanish['WINS'] = "victorias";		transArrSpanish['WIN'] = "derrotas";
			transArrSpanish['LOSSES'] = "victoria";		transArrSpanish['LOSS'] = "derrota";
			transArrGerman['WINS'] = "Siege";			transArrGerman['WIN'] = "Sieg";
			transArrGerman['LOSSES'] = "Verluste";		transArrGerman['LOSS'] = "Niederlage";
			transArrItalian['WINS'] = "Vittorie";		transArrItalian['WIN'] = "WIN";
			transArrItalian['LOSSES'] = "Sconfitte";	transArrItalian['LOSS'] = "PERDITA";
			transArrPortuguese['WINS'] = "Vitórias";	transArrPortuguese['WIN'] = "Derrotas";
			transArrPortuguese['LOSSES'] = "Vitória";	transArrPortuguese['LOSS'] = "derrota";
			
			transArrEnglish['CAN_YOU_DO_BETTER'] = "Can you do better?";
			transArrFrench['CAN_YOU_DO_BETTER'] = "Pouvez-vous le battre?";
			transArrSpanish['CAN_YOU_DO_BETTER'] = "¿Puede usted hacer mejor?";
			transArrGerman['CAN_YOU_DO_BETTER'] = "Können Sie es besser machen?";
			transArrItalian['CAN_YOU_DO_BETTER'] = "Puoi fare di meglio?";
			transArrPortuguese['CAN_YOU_DO_BETTER'] = "Você pode fazer melhor?";
			
			
			transArrEnglish['GAME_TYPE_INTRO'] = ""; // Play against yourself or compete with friends to journey up the leaderboard and discover the true VOCABULARIOUS amongst you.";
			transArrFrench['GAME_TYPE_INTRO'] = ""; //Jouez contre vous-même ou concurrencez des amis pour attenir le sommet de l’échelle des champions et découvrir le vrai VOCABULARIOUS parmi vous.";
			transArrSpanish['GAME_TYPE_INTRO'] = ""; //Juega contra sí mismo o para competir con sus amigos para viajar en la clasificación y descubrir la verdad VOCABULARIOUS entre ustedes.";
			transArrGerman['GAME_TYPE_INTRO'] = ""; //Gioca contro te stesso o sfida i tuoi amici per scalare la classifica e scoprire il vero VOCABULARIOUS tra voi.";
			transArrItalian['GAME_TYPE_INTRO'] = ""; //Spiele gegen dich selber oder schmeiß dich mit deinen Freunden die Rangliste hoch und findet heraus wer der wahre VOCABULARIOUS von euch ist!";
			transArrPortuguese['GAME_TYPE_INTRO'] = ""; //Jogue contra si mesmo ou compita com amigos para escalar o ranque e descobrir o verdadeiro vocabularious entre vocês.";
			
			
			
			transArrEnglish['VOCAB_KEYS_INSTRUCTION'] = "SUBMIT WORD: ENTER\n\nSHUFFLE LETTERS: SPACE BAR\n\nDELETE: BACKSPACE / DELETE\n\nCHANGE SET OF LETTERS: CTRL";
			transArrFrench['VOCAB_KEYS_INSTRUCTION'] = "SOUMETTRE WORD: ENTRER\nLETTRES SHUFFLE: BARRE D'ESPACE\nDELETE: Retour arrière / Suppr\nCHANGEMENT ensemble de lettres: CTRL";
			transArrSpanish['VOCAB_KEYS_INSTRUCTION'] = "ENVIAR LA PALABRA: ENTRAR\nCARTAS SHUFFLE: barra espaciadora\nDELETE: Retroceso / Eliminar\nCAMBIO DE JUEGO DE CARTAS: CTRL";
			transArrGerman['VOCAB_KEYS_INSTRUCTION'] = "SUBMIT WORD: ENTER\nSHUFFLE BRIEFE: SPACE BAR\nDELETE: BACKSPACE / DELETE\nCHANGE Satz von Buchstaben: STRG";
			transArrItalian['VOCAB_KEYS_INSTRUCTION'] = "INVIA PAROLA: INVIO\nLETTERE SHUFFLE: BARRA SPAZIO\nDELETE: BACKSPACE / DELETE\nCAMBIO SET DI LETTERE: CTRL";
			transArrPortuguese['VOCAB_KEYS_INSTRUCTION'] = "ENVIE A PALAVRA: ENTER\nLETRAS SHUFFLE: ESPAÇO BAR\nDELETE: BACKSPACE / DELETE\nMUDANÇA SET DE CARTAS: CTRL";
			
			
			transArrEnglish['VOCAB_KEYS_GRID'] = "SHUFFLE LETTERS: SPACE BAR\nENTER: ENTER";
			transArrFrench['VOCAB_KEYS_GRID'] = "SOUMETTRE WORD: ENTRER\nLETTRES SHUFFLE: BARRE D'ESPACE";
			transArrSpanish['VOCAB_KEYS_GRID'] = "ENVIAR LA PALABRA: ENTRAR\nCARTAS SHUFFLE: barra espaciadora";
			transArrGerman['VOCAB_KEYS_GRID'] = "SUBMIT WORD: ENTER\nSHUFFLE BRIEFE: SPACE BAR";
			transArrItalian['VOCAB_KEYS_GRID'] = "INVIA PAROLA: INVIO\nLETTERE SHUFFLE: BARRA SPAZIO";
			transArrPortuguese['VOCAB_KEYS_GRID'] = "ENVIE A PALAVRA: ENTER\nLETRAS SHUFFLE: ESPAÇO BAR";
			
						
			transArrEnglish['SCORED'] = "Scored";
			transArrFrench['SCORED'] = "Marqué";
			transArrSpanish['SCORED'] = "Anotó";
			transArrGerman['SCORED'] = "Erzielt";
			transArrItalian['SCORED'] = "Ha segnato";
			transArrPortuguese['SCORED'] = "Pontuação";
			
			
			transArrEnglish['REMOVED'] = "Removed";
			transArrFrench['REMOVED'] = "Suppression";
			transArrSpanish['REMOVED'] = "Eliminado";
			transArrGerman['REMOVED'] = "Removed";
			transArrItalian['REMOVED'] = "Rimosso";
			transArrPortuguese['REMOVED'] = "Removidas";
			
			transArrEnglish['IGNORED'] = "Ignored";
			transArrFrench['IGNORED'] = "Ignoré";
			transArrSpanish['IGNORED'] = "Ignorado";
			transArrGerman['IGNORED'] = "Ignoriert";
			transArrItalian['IGNORED'] = "Ignorato";
			transArrPortuguese['IGNORED'] = "Ignorado";
			
			
			
			transArrEnglish['WON_TO'] = "Won against";
			transArrFrench['WON_TO'] = "Won contre";
			transArrSpanish['WON_TO'] = "Le ganó a";
			transArrGerman['WON_TO'] = "gewonnen";
			transArrItalian['WON_TO'] = "Vinto contro";
			transArrPortuguese['WON_TO'] = "Ganhou de";
			
			
			transArrEnglish['LOST_TO'] = "Lost to";
			transArrFrench['LOST_TO'] = "Perdu à";
			transArrSpanish['LOST_TO'] = "Perdido";
			transArrGerman['LOST_TO'] = "verloren";
			transArrItalian['LOST_TO'] = "Perso contro";
			transArrPortuguese['LOST_TO'] = "Perdeu de";
			
			
			transArrEnglish['TIED_TO'] = "Tied with";
			transArrFrench['TIED_TO'] = "À égalité avec";
			transArrSpanish['TIED_TO'] = "Empatado con";
			transArrGerman['TIED_TO'] = "Tied mit";
			transArrItalian['TIED_TO'] = "Legato con";
			transArrPortuguese['TIED_TO'] = "Empatado com";
			
			
			transArrEnglish['CLASSIC_MODE'] = "Classic Mode";
			transArrFrench['CLASSIC_MODE'] = "Mode classique";
			transArrSpanish['CLASSIC_MODE'] = "Modo clásico";
			transArrGerman['CLASSIC_MODE'] = "Classic Modus";
			transArrItalian['CLASSIC_MODE'] = "Modalità Classic";
			transArrPortuguese['CLASSIC_MODE'] = "Modo clássico";
			
			transArrEnglish['SPEED_MODE'] = "Speed Mode";
			transArrFrench['SPEED_MODE'] = "Mode vitesse";
			transArrSpanish['SPEED_MODE'] = "Modo de velocidad";
			transArrGerman['SPEED_MODE'] = "Speed ​​Mode";
			transArrItalian['SPEED_MODE'] = "Modalità Velocità";
			transArrPortuguese['SPEED_MODE'] = "Modo de velocidade";
			
			transArrEnglish['GRID_MODE'] = "Grid Mode";
			transArrFrench['GRID_MODE'] = "Mode Grille";
			transArrSpanish['GRID_MODE'] = "Modo de red de";
			transArrGerman['GRID_MODE'] = "Grid Modus";
			transArrItalian['GRID_MODE'] = "Modalità griglia";
			transArrPortuguese['GRID_MODE'] = "Modo de grade";
			
			transArrEnglish['PLAY_AGAIN'] = "PLAY AGAIN";
			transArrFrench['PLAY_AGAIN'] = "Rejouer";
			transArrSpanish['PLAY_AGAIN'] = "Jugar de nuevo";
			transArrGerman['PLAY_AGAIN'] = "Play Again";
			transArrItalian['PLAY_AGAIN'] = "Gioca ancora";
			transArrPortuguese['PLAY_AGAIN'] = "Jogar novamente";
			
			
			transArrEnglish['YOU_WON'] = "You Won!";
			transArrFrench['YOU_WON'] = "vous avez gagné";
			transArrSpanish['YOU_WON'] = "que ganó";
			transArrGerman['YOU_WON'] = "Du hast gewonnen!";
			transArrItalian['YOU_WON'] = "hai vinto";
			transArrPortuguese['YOU_WON'] = "Você ganhou";
						
			transArrEnglish['YOU_LOST'] = "You Lost!";
			transArrFrench['YOU_LOST'] = "vous avez perdu";
			transArrSpanish['YOU_LOST'] = "¿Ha perdido";
			transArrGerman['YOU_LOST'] = "Du hast verloren!";
			transArrItalian['YOU_LOST'] = "hai perso";
			transArrPortuguese['YOU_LOST'] = "Você perdeu";
			
			transArrEnglish['FRIENDS'] = "FRIENDS";
			transArrFrench['FRIENDS'] = "Amis";
			transArrSpanish['FRIENDS'] = "amigos";
			transArrGerman['FRIENDS'] = "Freunde";
			transArrItalian['FRIENDS'] = "Amici";
			transArrPortuguese['FRIENDS'] = "Amigos";
			
			transArrEnglish['WORLD'] = "WORLD";
			transArrFrench['WORLD'] = "Monde";
			transArrSpanish['WORLD'] = "Mundo";
			transArrGerman['WORLD'] = "Welt";
			transArrItalian['WORLD'] = "Mondo";
			transArrPortuguese['WORLD'] = "Mundo";
			
			/*
			transArrEnglish[''] = "";
			transArrFrench[''] = "";
			transArrSpanish[''] = "";
			transArrGerman[''] = "";
			transArrItalian[''] = "";
			transArrPortuguese[''] = "";
			*/
			_translationsArr.push(transArrEnglish);
			_translationsArr.push(transArrFrench);
			_translationsArr.push(transArrSpanish);
			_translationsArr.push(transArrItalian);
			_translationsArr.push(transArrGerman);
			_translationsArr.push(transArrPortuguese);
			
			
			////////////////////
			// Instructions
			//
			
			// English
			var instructionsLangArrEnglish:Array = new Array();
			
			instructionsLangArrEnglish.push("Seven letters, countless possibilities. \nThe race is on to identify the words hidden in the letter jumble.\nForm a word choosing or typing the letters.\nYou can change the 'letter set' once 5 or more words are found using the 'ABC' button.\Click the ‘Tick’ or just press the ‘enter’ key to submit your word.")
			instructionsLangArrEnglish.push("-Leaderboard\n-Click on a friend to compete and discover who is the most vocabularious amongst you\n-Friend vs. friend");
			instructionsLangArrEnglish.push("- Identify the words hidden in the letter grid before the clock runs out. The more you find, the higher you score. The letters in the grid transform every 20 seconds…so move at the speed of light.");
			instructionsLangArrEnglish.push("-Connect the letters in the grid to discover the hidden words.");
			
			_instructionsLangArr.push(instructionsLangArrEnglish);
			
			// French
			var instructionsLangArrFrench:Array = new Array();
			
			instructionsLangArrFrench.push("Identifiez les mots cachés dans la grille de lettre avant que l'horloge s'épuise. Le plus de mots trouver, le plus vos points. Cliquez n'importe quelle lettre sur l’écran ou votre clavier pour commencer. Pressez ‘Tick’ ou ‘Enter’ pour soumettre votre mot, et ‘X’ pour régénérer les lettres.");
			instructionsLangArrFrench.push("- L’échelle des champions\n- Cliquez sur un ami pour le concurrencer et découvrir qui est le plus Vocabularious parmi vous\n- ami contre ami");
			instructionsLangArrFrench.push("- Identifiez les mots cachés dans la grille de lettre avant que l'horloge s'épuise. Le plus de mots trouver, le plus vos points. Mais cette fois çi, les lettres se transforment toutes les 20 secondes…alors jouez à la vitesse de la lumière!");
			instructionsLangArrFrench.push("Reliez les lettres dans l'hexagone pour découvrir les mots cachés.");
			
			_instructionsLangArr.push(instructionsLangArrFrench);
			
			// Spanish
			var instructionsLangArrSpanish:Array = new Array();
			
			instructionsLangArrSpanish.push("Identifica las palabras ocultas en la rejilla antes de que se agote el tiempo. Cuantas mas encuentres mas puntos conseguiras. ");
			instructionsLangArrSpanish.push("Siete letras, un sinfín de posibilidades. 'Play' de prensa y su cuenta atrás ... la carrera es el de identificar las palabras ocultas en la mezcla carta.");
			instructionsLangArrSpanish.push("Juega contra sí mismo o para competir con sus amigos para viajar en la clasificación y descubrir la verdad VOCABULARIOUS entre ustedes.");
			instructionsLangArrSpanish.push("Toca en cualquier letra o en el teclado para empezar. Pulsa 'Tocar' o 'Terminado' para enviar y 'X' para refrescar.");
			
			_instructionsLangArr.push(instructionsLangArrSpanish);
			
			// German
			var instructionsLangArrGerman:Array = new Array();
			
			instructionsLangArrGerman.push("Sieben Buchstaben, unzählige Möglichkeiten. Drücke “Start” und dein Countdown beginnt…das Rennen ums identifizieren der versteckten Wörter im Buchstaben-wirrwarr kann losgehen! \n Klicke auf irgendeinen Buchstaben oder eine beliebige Taste um zu beginnen. Drücke 'Tick' oder 'Enter' um abzuschicken.");
			instructionsLangArrGerman.push("Spiele gegen dich selber oder schmeiß dich mit deinen Freunden die Rangliste hoch und findet heraus wer der wahre VOCABULARIOUS von euch ist!");
			instructionsLangArrGerman.push("Identifizier die versteckten Wörter im Buchstabenraster bevor die Zeit abläuft. Umso mehr du findest, desto höher deine Punkteanzahl.");
			instructionsLangArrGerman.push("Verbinde die Buchstaben im Raster/Hexagon um die versteckten Wörter zu finden.");
			
			_instructionsLangArr.push(instructionsLangArrGerman);
			
			// Italian
			var instructionsLangArrItalian:Array = new Array();
			
			instructionsLangArrItalian.push("Trova le parole nascoste nella griglia di lettere prima che il tempo finisca. Più parole troverai, più alto sarà il tuo punteggio.");
			instructionsLangArrItalian.push("Sette lettere, infinite possibilità. Premi 'Play' e il conto alla rovescia apparirà… La sfida è di identificare le parole nascoste nel groviglio di lettere.");
			instructionsLangArrItalian.push("Gioca contro te stesso o sfida i tuoi amici per scalare la classifica e scoprire il vero VOCABULARIOUS tra voi.");
			instructionsLangArrItalian.push("Clicca su una lettera qualsiasi sulla tastiera per cominciare. Premi 'Tick' o 'Entra' per presentare e X per ripulire.");
			
			_instructionsLangArr.push(instructionsLangArrItalian);
			
			// Portuguese
			var instructionsLangArrPortuguese:Array = new Array();
			
			instructionsLangArrPortuguese.push("Identifique as palavras escondidas na grade de letras antes que o tempo se esgote. Quanto mais palavras encontrar, mais pontos você marca.\nSete letras, incontáveis possibilidades. pressione ‘Jogar’ e sua contagem aparecerá... a corrida está valendo para identificar as palavras escondidas nas letras embaralhadas.\nClique em qualquer letra ou digite no seu teclado para começar. Pressione ‘enter’ para submeter e ‘x’ para atualizar.");
			instructionsLangArrPortuguese.push("-Ranking de líderes\n-Clique num amigo para competir e descubra quem é mais vocabularious entre vocês.");
			instructionsLangArrPortuguese.push("Identifique as palavras escondidas na grade de letras antes que o tempo se esgote. Quanto mais palavras encontrar, mais pontos você marca. As letras na grade mudam a cada 20 segundos... Então mova-se na velocidade da luz.");
			instructionsLangArrPortuguese.push("Conecte as letras na Grade/Hexágono para descobrir as letras escondidas.");
			
			_instructionsLangArr.push(instructionsLangArrPortuguese);
			
			///////////////
			// Rank Names
			//
			
			// English
			var rankName_English:Array = new Array(13);
			var rankDesc_English:Array = new Array(13);
			rankName_English[1] = "ADMIRAL OF THE STAR FLEET";
			rankDesc_English[1] = "Congratulations Admiral of the Star Fleet! You have proven your galactic Vocabulariousness to us all.\nYou are now ready to command any battleship you wish.";
			rankName_English[2] = "ADMIRAL";
			rankDesc_English[2] = "Congratulations Admiral! There is just one rank above you now.\nPlay again and push harder and you could be in charge of the whole fleet.";
			rankName_English[3] = "COMMODORE";
			rankDesc_English[3] = "Congratulations on attaining Commodore! Just two levels to go till your Vocabulariousness makes you Admiral of the Star Fleet!";
			rankName_English[4] = "CAPTAIN";
			rankDesc_English[4] = "We salute you, Captain! You are highly Vocabularious indeed. You make your troops proud.\nPlay again to try for Commodore?";
			rankName_English[5] = "LIEUTENANT";
			rankDesc_English[5] = "Lieutenant Vocabularious, you impress us with your abilities. We see the making of an Admiral in you. There are just four levels left between you and command of the whole fleet.\nPlay again?";
			rankName_English[6] = "DOCTOR";
			rankDesc_English[6] = "Well played! We proudly promote you to Doctor Vocabularious.\nPlay again to reach for Lieutenant?";
			rankName_English[7] = "DROID";
			rankDesc_English[7] = "You know a scary amount of words. You must be a Droid.\nPlay again for another chance to command the Star Fleet?";
			rankName_English[8] = "STAR FIGHTER PILOT";
			rankDesc_English[8] = "A valiant effort. Congratulations, Star Fighter Pilot!\nYour brave Vocabulariousness could make you Captain soon…\nplay again?";
			rankName_English[9] = "CAPTAIN'S HEAD CHEF";
			rankDesc_English[9] = "Good game, Chef. You make the Captain proud.\nDo you have what it takes to become a fighter pilot?\nPlay again to see.";
			rankName_English[10] = "POT AND PAN CLEANER";
			rankDesc_English[10] = "Sometimes luck escapes us all...\nPlay again to prove your Vocabulariousness.";
			rankName_English[11] = "POTATO PEALER";
			rankDesc_English[11] = "Sometimes luck escapes us all...\nPlay again to prove your Vocabulariousness.";
			rankName_English[12] = "SCIENTIFIC TEST SUBJECT";
			rankDesc_English[12] = "Was luck against you this time?\nPlay again to prove your Vocabulariousness.";
			_rankingsLangArr.push(rankName_English);
			_rankDescLangArr.push(rankDesc_English);
			
			// French
			var rankName_French:Array = new Array(13);
			var rankDesc_French:Array = new Array(13);
			rankName_French[1] = "AMIRAL DE LA FLOTTE  DES ETOILES";
			rankDesc_French[1] = "Félicitation, Amiral de la flotte des étoiles! Vous avez prouvé vos talents de Vocabulariste galactique à vos pairs. Vous êtes maintenant prêt à commander le cuirassé de votre choix.";
			rankName_French[2] = "AMIRAL";
			rankDesc_French[2] = "Félicitation, Amiral! Il n'y a plus qu'un grade au dessus de vous. Jouez à nouveau et en faisant mieux, vous pourriez être au commandement de toute la flotte!";
			rankName_French[3] = "COMMANDANT ";
			rankDesc_French[3] = "Félicitation, vous avez gagné le grade de Commandant! Encore deux grades et vos aptitudes de Vocabulariste feront de vous Amiral de la flotte!";
			rankName_French[4] = "CAPITAINE";
			rankDesc_French[4] = "Le saludamos, capitán! Usted está muy Vocabularious hecho. Usted hace sus tropas orgulloso. Jugar de nuevo para tratar de Commodore?";
			rankName_French[5] = "LIEUTENANT";
			rankDesc_French[5] = "Lieutenant Vocabulariste, vos dons nous ont impressionné! Nous voyons en vous un futur Amiral. Seuls quatre niveaux vous séparent du commandement de toute la flotte. Voulez-vous jouer à nouveau?";
			rankName_French[6] = "DOCTEUR";
			rankDesc_French[6] = "Bien joué! Vous êtes fièrement promu Docteur Vocabulariste. Jouez à nouveau pour devenir Lieutenant.";
			rankName_French[7] = "DROID";
			rankDesc_French[7] = "Vous connaissez une quantité de mots effrayante. Vous devez être un droïd. Jouez encore pour gagner la chance de commander la flotte des étoiles.";
			rankName_French[8] = "PILOTE  DE COMBAT";
			rankDesc_French[8] = "Bel effort! Félicitation pilote de combat! Votre courage de Vocabulariste pourrait faire de vous un Capitaine très prochainement. Voulez-vous jouer à nouveau?";
			rankName_French[9] = "CHEF CUISINIER DU CAPITAINE";
			rankDesc_French[9] = "Bien joué, chef! Le capitaine est fière de vous. Avez-vous ce qu'il faut pour devenir un pilote de chasse? Jouez encore pour le découvrir!";
			rankName_French[10] = "CHEF DES CORVEES";
			rankDesc_French[10] = "Beaux progrès. Vous pourriez certainement gagner votre place au sommet! Jouez encore pour devenir Amiral.";
			rankName_French[11] = "EPLUCHEUR DE PATATES ";
			rankDesc_French[11] = "Nous souffrons tous d'un manque de chance quelques fois. Jouez encore pour prouvez vos dons de Vocabulariste.";
			rankName_French[12] = "COBAYE DE LABORATOIRE";
			rankDesc_French[12] = "On joue de malchance? Jouez encore pour prouvez vos dons de Vocabulariste.";
			_rankingsLangArr.push(rankName_French);
			_rankDescLangArr.push(rankDesc_French);
			
			// Spanish
			var rankName_Spanish:Array = new Array(13);
			var rankDesc_Spanish:Array = new Array(13);
			rankName_Spanish[1] = "Almirante de la Flota Estelar";
			rankDesc_Spanish[1] = "Felicidades almirante de la flota de la estrella! Que han demostrado su Vocabulariousness galáctica para todos nosotros. Ahora está listo para comandar cualquier barco de guerra que desee.";
			rankName_Spanish[2] = "ADMIRAL";
			rankDesc_Spanish[2] = "Felicidades Almirante! Sólo hay un rango por encima de ti. Jugar de nuevo y presionar con más fuerza y que podría estar a cargo de toda la flota.";
			rankName_Spanish[3] = "COMMODORE";
			rankDesc_Spanish[3] = "Parabéns por alcançar Commodore! Apenas dois níveis para ir até o seu Vocabulariousness faz você almirante da frota Star!";
			rankName_Spanish[4] = "CAPITAN";
			rankDesc_Spanish[4] = "Le saludamos, capitán! Usted está muy Vocabularious hecho. Usted hace sus tropas orgulloso. Jugar de nuevo para tratar de Commodore?";
			rankName_Spanish[5] = "TENIENTE";
			rankDesc_Spanish[5] = "Vocabularious teniente, que nos impresiona con sus habilidades. Vemos la realización de un almirante en ti. Sólo hay cuatro niveles que queda entre usted y el mando de la flota. Jugar de nuevo?";
			rankName_Spanish[6] = "ESTRELLA médico de a bordo";
			rankDesc_Spanish[6] = "Bien jugado! Estamos orgullosos de promocionar a Vocabularious Doctor. Juega de nuevo para llegar a teniente.";
			rankName_Spanish[7] = "DROID TÉCNICO";
			rankDesc_Spanish[7] = "Usted sabe la cantidad de miedo de las palabras. Usted debe ser un Droid. Jugar de nuevo para otra oportunidad de comandar la flota de la estrella.";
			rankName_Spanish[8] = "ESTRELLA piloto de caza";
			rankDesc_Spanish[8] = "Un valiente esfuerzo. Felicidades, piloto Star Fighter! Su Vocabulariousness valiente podría hacer que el capitán pronto ... volver a jugar?";
			rankName_Spanish[9] = "CHEF CAPITÁN CABEZA";
			rankDesc_Spanish[9] = "Buen juego, Chef. Usted hace el capitán orgulloso. ¿Tienes lo necesario para convertirse en un piloto de caza? Juega de nuevo para ver.";
			rankName_Spanish[10] = "POT y más limpio PAN";
			rankDesc_Spanish[10] = "A veces la suerte se nos escapa a todos .... Tócala otra vez para demostrar su Vocabulariousness.";
			rankName_Spanish[11] = "Pelapatatas";
			rankDesc_Spanish[11] = "A veces la suerte se nos escapa a todos .... Tócala otra vez para demostrar su Vocabulariousness.";
			rankName_Spanish[12] = "Sujeto de la prueba CIENTÍFICA";
			rankDesc_Spanish[12] = "Fue una suerte que en contra de este tiempo? Tócala otra vez para demostrar su Vocabulariousness.";
			_rankingsLangArr.push(rankName_Spanish);
			_rankDescLangArr.push(rankDesc_Spanish);
			
			
			// German
			var rankName_German:Array = new Array(13);
			var rankDesc_German:Array = new Array(13);
			rankName_German[1] = "ADMIRAL DER STERNENFLOTTE";
			rankDesc_German[1] = "Gratulation, Admiral der Sternenflotte! Sie haben uns allen ihre galaktische Vocabulariousness gezeigt! Sie sind nun bereit ein Schiff ihrer Wahl zu kommandieren!";
			rankName_German[2] = "ADMIRAL";
			rankDesc_German[2] = "Gratulation Admiral! Nur noch ein Rang über Ihnen! Spielen Sie nochmal und geben Sie Ihr Bestes und Sie könnten die komplette Flotte leiten!";
			rankName_German[3] = "KOMMODORE";
			rankDesc_German[3] = "Sie haben den Kommodore Rang erhalten, Gratulation! Noch zwei Level bis Ihre Vocabulariousness Sie zum Admiral der Sternenflotte macht!";
			rankName_German[4] = "SCHIFFSKAPITÄN";
			rankDesc_German[4] = "Wir begrüßen Sie, Kapitän! Sie sind tatsächlich hoch Vocabularious! Sie machen Ihre Truppen stolz. Nochmal um Kommodore spielen?";
			rankName_German[5] = "LEUTNANT";
			rankDesc_German[5] = "Leutnant Vocabularious, Sie beeindrucken uns mit Ihren Fähigkeiten. Wir sehen die Geburt eines Admirals in Ihnen. Es fehlen noch vier Level bis Sie Kontrolle über die ganze Flotte bekommen! Nochmal spielen?";
			rankName_German[6] = "STERNENSCHIFF DOKTOR";
			rankDesc_German[6] = "Gut gespielt! Stolz befördern wir Sie zu Doktor Vocabularious. Spielen Sie nochmal um Leutnant zu erreichen!";
			rankName_German[7] = "DRUIDENTECHNIKER";
			rankDesc_German[7] = "Sie kennen eine unheimliche Menge an Wörtern. Sie müssen ein Druide sein. Spielen Sie nochmal für eine weitere Chance die Sternenflotte zu kommandieren!";
			rankName_German[8] = "STERNENJÄGERPILOT";
			rankDesc_German[8] = "Eine tapfere Leistung. Gratulation, Sternenjägerpilot! Ihre mutige Vocabulariousness könnte Sie bald zum Kapitän befördern... nochmal spielen?";
			rankName_German[9] = "KAPITÄN’S CHEFKOCH";
			rankDesc_German[9] = "Gutes Spiel, Chef. Sie machen den Kapitän stolz. Habt Ihr das Zeug um ein Jägerpilot zu werden? Dann spiele nochmal!";
			rankName_German[10] = "GESCHIRRWÄSCHER";
			rankDesc_German[10] = "Guter Fortschritt. Wir glauben dass dein Vocabulariousness sehr bald die Spitze der Ränge erreicht. Nochmal spielen und Admiral versuchen?";
			rankName_German[11] = "KARTOFFELSCHÄLER";
			rankDesc_German[11] = "Das Glück verlässt uns alle manchmal... Spiele nochmal um deine Vocabulariousness zu beweisen.";
			rankName_German[12] = "WISSENSCHAFTLICHES TESTOBJEKT";
			rankDesc_German[12] = "War das Glück diesesmal gegen dich? Spiele nochmal um deine Vocabulariousnes zu beweisen.";
			_rankingsLangArr.push(rankName_German);
			_rankDescLangArr.push(rankDesc_German);
			
			
			// Italian
			var rankName_Italian:Array = new Array(13);
			var rankDesc_Italian:Array = new Array(13);
			rankName_Italian[1] = "ADMIRAL DI STARFLEET";
			rankDesc_Italian[1] = "Complimenti Ammiraglio della Flotta Stellare! Hai dimostrato il tuo Vocabulariousness galattico a tutti noi. Ora siete pronto a comandare qualsiasi nave da guerra che desiderate..";
			rankName_Italian[2] = "ADMIRAL";
			rankDesc_Italian[2] = "Complimenti Ammiraglio! C'è solo un grado sopra di voi ora. Giocate ancora e spingerte di più e potrete essere il responsabile di tutta la flotta.";
			rankName_Italian[3] = "COMMODORE";
			rankDesc_Italian[3] = "Congratulazioni per il raggiungimento Commodore! Solo due livelli per andare fino al vostro Vocabularious farà di voi Ammiraglio della Flotta Stellare!";
			rankName_Italian[4] = "CAPITANO DI BORDO";
			rankDesc_Italian[4] = "Vi saluto, Capitano! Voi siete davvero molto Vocabularious. Rendete orgogliose le vostre truppe. Giocare di nuovo per provare come Commodore?";
			rankName_Italian[5] = "TENENTE";
			rankDesc_Italian[5] = "Vocabularious Tenente, ci impressionare con la vostra abilità. Vediamo crescere un ammiraglio in voi. Ci sono solo quattro livelli che separano voi e il comando della flotta. Gioca ancora?";
			rankName_Italian[6] = "STAR SHIP MEDICO";
			rankDesc_Italian[6] = "Ben fatto! Siamo orgogliosi di promuovere a Dottor Vocabularious. Gioca ancora per raggiungere il ruolo di tenente.";
			rankName_Italian[7] = "DROID TECNICO";
			rankDesc_Italian[7] = "Sapete una quantità spaventosa di parole. Devi essere un Droide. Gioca ancora per un'altra possibilità di comandare la flotta Stellare.";
			rankName_Italian[8] = "PILOTA STELLARE COMBATTENTE";
			rankDesc_Italian[8] = "Uno sforzo coraggioso. Congratulazioni, Pilot Star Fighter! Il tuo Vocabulariousness coraggiosa potrebbe farvi capitano presto ... giocare ancora?";
			rankName_Italian[9] = "CAPO CUOCO";
			rankDesc_Italian[9] = "Bella partita, Chef. Rendete orgoglioso il capitano. Hai quello che serve per diventare un pilota di caccia? Gioca ancora per scoprirlo.";
			rankName_Italian[10] = "POT E DETERGENTE PAN";
			rankDesc_Italian[10] = "Bel progesso.  Il tuo Vocabulariousness potrebbe essere presto al top delle classifiche. Gioca ancora o prova per Ammiraglio?";
			rankName_Italian[11] = "PELAPATATE";
			rankDesc_Italian[11] = "A volte la fortuna ci sfugge a tutti. Gioca ancora per dimostrare il tuo Vocabulariousness.";
			rankName_Italian[12] = "TEST argomento scientifico";
			rankDesc_Italian[12] = "È stata fortuna contro di voi questa volta? Gioca ancora per dimostrare il tuo Vocabulariousness.";
			_rankingsLangArr.push(rankName_Italian);
			_rankDescLangArr.push(rankDesc_Italian);
						
			// Portuguese
			var rankName_Portuguese:Array = new Array(13);
			var rankDesc_Portuguese:Array = new Array(13);
			rankName_Portuguese[1] = "ALMIRANTE DA FROTA ESTELAR";
			rankDesc_Portuguese[1] = "Parabéns Almirante da Frota Estelar! Você provou para todos nós que tem um vocabulário galáctico. Agora você está pronto para comandar qualquer navio de guerra que desejar.  ";
			rankName_Portuguese[2] = "Almirante";
			rankDesc_Portuguese[2] = "Parabéns Almirante! Há apenas uma posição acima de você agora. Jogue novamente, com mais vontade ainda, e você poderá se tornar responsável por toda a frota.";
			rankName_Portuguese[3] = "COMODORO";
			rankDesc_Portuguese[3] = "Parabéns pela conquista Comodoro! Apenas dois níveis até que seu vocabulário o torne Almirante da Frota Estelar!";
			rankName_Portuguese[4] = "CAPITÃO DO NAVIO";
			rankDesc_Portuguese[4] = "Parabéns Capitão! Seu vocabulário é, de fato, muito bom. Sua tropa está orgulhosa. Jogar novamente para tentar se tornar Comodoro?";
			rankName_Portuguese[5] = "TENENTE";
			rankDesc_Portuguese[5] = "Tenente Vocabularious, você nos impressiona com suas habilidades. Vemos um Almirante se criando em você. Há apenas quatro níveis restantes entre você e o comando de toda a frota. Jogar novamente?";
			rankName_Portuguese[6] = "MÉDICO DO NAVIO ESTELAR";
			rankDesc_Portuguese[6] = "Bem Jogado! Temos orgulho em promovê-lo a Doutor Vocabularious. Jogue novamente para chegar a Tenente. ";
			rankName_Portuguese[7] = "Técnico DROID ";
			rankDesc_Portuguese[7] = "Você conhece uma quantidade assustadora de palavras. Você deve ser um (Droid?). Jogar novamente para outra chance de comandar a frota Estela.";
			rankName_Portuguese[8] = "PILOTO DE CAÇA ESTELAR";
			rankDesc_Portuguese[8] = "Um grande esforço. Parabéns, Piloto de Caça Estelar! Seu vocabulário corajoso pode torná-lo Capitão logo... Jogar novamente?";
			rankName_Portuguese[9] = "CHEFE DE COZINHA DO CAPITÃO";
			rankDesc_Portuguese[9] = "Bom jogo, Chefe. O capitão está orgulhoso de você. Você tem o necessário para se tornar um piloto de caça? Jogue novamente para ver. ";
			rankName_Portuguese[10] = "LAVADOR DE PRATOS";
			rankDesc_Portuguese[10] = "Bom progresso. Acreditamos que você, com seu vocabulário, estará, em breve, no topo do ranque. Jogar novamente para tentar se tornar Almirante?";
			rankName_Portuguese[11] = "DESCASCADOR DE BATATAS";
			rankDesc_Portuguese[11] = "Às vezes a sorte nos escapa tudo .... Jogar de novo para provar sua Vocabulariousness.";
			rankName_Portuguese[12] = "OBJETO DE TESTE CIENTÍFICO";
			rankDesc_Portuguese[12] = "Desta vez a sorte não estava ao seu lado? Jogue novamente para provar seu vocabulário.";
			_rankingsLangArr.push(rankName_Portuguese);
			_rankDescLangArr.push(rankDesc_Portuguese);
			
			/////////////////
			// Stream Invite
			//
			
			// English
			_feedInviteArr.push("Come and challange me on Vocabularious");
			// FRENCH
			_feedInviteArr.push("Venez Challange moi sur Vocabularious");
			// SPANISH
			_feedInviteArr.push("Cliquez pour inviter");
			// GERMAN
			_feedInviteArr.push("Komm und schlage meine Punkteanzahl in Vocabularious");
			// ITALIAN
			_feedInviteArr.push("Vieni e sfida mi Vocabularious");
			// PORTUGUESE
			_feedInviteArr.push("Venha e supere minha pontuação no Vocabularious");
			
			/////////////////
			// Stream Score
			//
			
			// English
			_feedScoreTitleArr.push("Come and beat my score on Vocabularious");
			_feedScoreTextArr.push("<<name>> achieved <<score>> on Vocabularious, earning a Captaincy. Earn a badge of your own when you accept the challenge to identify the missing words.");
			// FRENCH
			_feedScoreTitleArr.push("Venez Challange moi sur Vocabularious");
			_feedScoreTextArr.push("<<name>> a gagné son rang de Capitaine en marquant <<score>> points à Vocabularious. Gagnez vos propres gallons en acceptant le défi de reconnaître le mots manquant.");
			// SPANISH
			_feedScoreTitleArr.push("Ven y golpearon a mi cuenta en Vocabularious");
			_feedScoreTextArr.push("<<name>> logró en <<score>> Vocabularious, ganando una Capitanía. Ganar una medalla de su cuenta cuando usted acepta el desafío de identificar las palabras que faltan.");
			// GERMAN
			_feedScoreTitleArr.push("Komm und schlage meine Punkteanzahl in Vocabularious");
			_feedScoreTextArr.push("<<name>> erzielte <<score>> auf Vocabularious, verdienen Kapitanat. Verdienen Sie ein Zeichen der eigenen, wenn Sie die Herausforderung an, die fehlenden Wörter zu identifizieren akzeptieren.");
			// ITALIAN
			_feedScoreTitleArr.push("Vieni e batty il mio punteggio su Vocabulatious");
			_feedScoreTextArr.push("<<name>> ottenuto il punteggio di <<score>> su Vocabularious, guadagnando il grado di Capitano. Guadagna un distintivo personale quando accetti la sfida per identificare le parole mancanti.");
			// PORTUGUESE
			_feedScoreTitleArr.push("Vir e bater a minha pontuação no Vocabularious");
			_feedScoreTextArr.push("Scott alcançou 1234 pontos no Vocabularious, ganhando a capitania. Conquiste seu próprio distintivo aceitando o desafio para identificar as palavras perdidas.");
			
			/////////////////
			// Stream Challenge
			//
			
			// English
			_feedChallengeTitleArr.push("Confirm Challenge");
			_feedChallengeArr.push("Captain <<name>> has scored <<score>> on Vocabularious. Do you dare accept the challenge to beat their score and discover your rank?");
			// FRENCH
			_feedChallengeTitleArr.push("Confirmer Défi");
			_feedChallengeArr.push("Capitaine <<name>> a marqué <<score>> points à Vocabularious. Relevez-vous le défi de battre son score et de découvrir votre rang?");
			// SPANISH
			_feedChallengeTitleArr.push("Confirmar Desafío");
			_feedChallengeArr.push("El capitán <<name>> ha marcado <<score>> en Vocabularious. ¿Te atreves a aceptar el reto de superar su puntuación y descubre su rango?");
			// GERMAN
			_feedChallengeTitleArr.push("Bestätigen Herausforderung");
			_feedChallengeArr.push("Kapitän  <<name>> hat <<score>> punkte bei Vocabularious. Wagst du es die Herausforderung anzunehmen um seine Punkte zu schlagen und deinen Rang herauszufinden?");
			// ITALIAN
			_feedChallengeTitleArr.push("Conferma Sfida");
			_feedChallengeArr.push("Capitano <<name>> ha ottenuto il punteggio di <<score>> su Vocabularious. Osate accettare la sfida per battere il suo punteggio e scoprire tuo grado?");
			// PORTUGUESE
			_feedChallengeTitleArr.push("Confirmar Desafio");
			_feedChallengeArr.push("Capitão <<name>>  marcou <<score>> pontos no Vocabulorious. Você ousa aceitar o desafio para superar sua pontuação e descobir seu ranque?");
		}
		
		public function getTranslation(sLookup:String):String
		{
			return _translationsArr[_selectedLanguage][sLookup];
		}
		
		public function getInstructions(reference:uint):String
		{
			return _instructionsLangArr[_selectedLanguage][reference];
		}
		
		public function getRankName(rank:int):String
		{
			return _rankingsLangArr[_selectedLanguage][rank];
		}
		
		public function getRankDesc(rank:uint):String
		{
			return _rankDescLangArr[_selectedLanguage][rank];
		}
		
		public function getInviteFeed():String
		{
			return _feedInviteArr[_selectedLanguage];
		}
		
		public function getScoreTitleFeed():String
		{
			return _feedScoreTitleArr[_selectedLanguage];
		}
		
		public function getScoreTextFeed(sName:String, score:uint):String
		{
			var sOut:String = _feedScoreTextArr[_selectedLanguage];
			
			trace("getScoreTextFeed:" + sOut);
			
			sOut = sOut.replace("<<score>>", String(score));
			sOut = sOut.replace("<<name>>", sName);
			trace("getScoreTextFeed-post:" + sOut);
			
			return sOut;
		}
		
		public function getChallengeTitleFeed():String
		{
			return _feedChallengeTitleArr[_selectedLanguage];
		}
		
		public function getChallengeFeed(sName:String, score:uint):String
		{
			var sOut:String = _feedChallengeArr[_selectedLanguage];
			sOut = sOut.replace("<<score>>", String(score));
			sOut = sOut.replace("<<name>>", sName);
						
			return sOut;
		}
		
		public function setLanguage(selectedLanguage:uint):void
		{
			_selectedLanguage = selectedLanguage;
		}
		
		public function getLanguage():uint
		{
			return _selectedLanguage;
		}
	}
}