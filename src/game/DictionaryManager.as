﻿package game
{
	import util.ArrayList;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;
	import flash.net.URLRequest;
	import flashx.textLayout.utils.CharacterUtil;
	
	import game.Dictionary;
	
	public class DictionaryManager extends EventDispatcher
	{
		
		// Defaults
		
		private static const DEFAULT_DICTIONARY:String = "en_GB";
		
		// Dictionary List
		
		private var _dictionaryList:Array = new Array();
		
		// Current Dictionary
		
		public var _currDictionary:Dictionary = null;
		
		// Singleton

		private static var instance:DictionaryManager = new DictionaryManager();
		
		public function DictionaryManager()
		{
			if(instance)
			{
				throw new Error ("We cannot create a new instance. Please use DictionaryManager.getInstance()");
			}
			else
			{
				loadDictionaryList();
				
				//loadDictionary( DEFAULT_DICTIONARY );
			}
		}

		public static function getInstance():DictionaryManager
		{
			return instance;
		}

		public function loadDictionaryList() : void
		{
			/*
			 * Old style of storing dictionaries internally
			 *
			_dictionaryList.push( new Dictionary( "Deutsch", "de_DE", new Dictionary_de_DE() ) );
			_dictionaryList.push( new Dictionary( "English UK", "en_GB", new Dictionary_en_GB() ) );
			_dictionaryList.push( new Dictionary( "English US", "en_US", new Dictionary_en_US() ) );
			_dictionaryList.push( new Dictionary( "English TWL", "TWL", new Dictionary_TWL() ) );
			_dictionaryList.push( new Dictionary( "English SOWPODS", "SOWPODS", new Dictionary_SOWPODS() ) );
			_dictionaryList.push( new Dictionary( "Espanol", "es_ES", new Dictionary_es_ES() ) );
			_dictionaryList.push( new Dictionary( "Francais", "fr_FR", new Dictionary_fr_FR() ) );
			_dictionaryList.push( new Dictionary( "Francais OSD", "OSD", new Dictionary_OSD() ) );
			_dictionaryList.push( new Dictionary( "Italiano", "it_IT", new Dictionary_it_IT() ) );
			_dictionaryList.push( new Dictionary( "Italiano ZINGA", "ZINGA", new Dictionary_ZINGA() ) );
			_dictionaryList.push( new Dictionary( "Nederlands", "nl_NL", new Dictionary_nl_NL() ) );
			*/
			
			
			_dictionaryList.push( new Dictionary( "English UK", "en_GB", null, 7) );
			_dictionaryList.push( new Dictionary( "English US", "en_US", null, 8) );
						
			_dictionaryList.push( new Dictionary( "Espanol", "es_ES", null, 9 ) );
			_dictionaryList.push( new Dictionary( "Francais", "fr_FR", null, 10 ) );
			_dictionaryList.push( new Dictionary( "Italiano", "it_IT", null, 11 ) );
			//_dictionaryList.push( new Dictionary( "Deutsch", "de_DE", null, 12 ) );
			
			
			_dictionaryList.push( new Dictionary( "Portuguese", "PT", null, 13) );
			
			
			/*_dictionaryList.push( new Dictionary( "English TWL", "TWL", null ) );
			_dictionaryList.push( new Dictionary( "English SOWPODS", "SOWPODS", null ) );
			_dictionaryList.push( new Dictionary( "Deutsch", "de_DE", null ) );
			_dictionaryList.push( new Dictionary( "Francais OSD", "OSD", null ) );
			
			_dictionaryList.push( new Dictionary( "Italiano ZINGA", "ZINGA", null ) );
			_dictionaryList.push( new Dictionary( "Nederlands", "nl_NL", null ) );
			*/
		}
		
		public function getDictionaryList() : Array
		{
			return _dictionaryList;
		}
		
		public function getCurrDictionary() : Dictionary
		{
			return _currDictionary;
		}
		
		public function loadDictionary( name:String ): Dictionary
		{
			var result:Dictionary = null;
			
			// Load Permutations
			/*
			if ( PermutationManager.getInstance().loadPermutation( name ) == false )
			{
				return result;
			}
			
			
			switch( name )
			{
				case "de_DE":
				{
					result = new Dictionary( "Deutsch", "de_DE", new Dictionary_de_DE() );
					break;
				}
				case "en_GB":
				{
					result = new Dictionary( "English UK", "en_GB", new Dictionary_en_GB() );
					break;
				}
				case "en_US":
				{
					result = new Dictionary( "English US", "en_US", new Dictionary_en_US() );
					break;
				}
				case "es_ES":
				{
					result = new Dictionary( "Espanol", "es_ES", new Dictionary_es_ES() );
					break;
				}
				case "fr_FR":
				{
					result = new Dictionary( "Francais", "fr_FR", new Dictionary_fr_FR() );
					break;
				}
				case "it_IT":
				{
					result = new Dictionary( "Italiano", "it_IT", new Dictionary_it_IT() );
					break;
				}
				case "nl_NL":
				{
					result = new Dictionary( "Nederlands", "nl_NL", new Dictionary_nl_NL() );
					break;
				}
				case "OSD":
				{
					result = new Dictionary( "Francais OSD", "OSD", new Dictionary_OSD() );
					break;
				}
				case "SOWPODS":
				{
					result = new Dictionary( "English SOWPODS", "SOWPODS", new Dictionary_SOWPODS() );
					break;
				}
				case "TWL":
				{
					result = new Dictionary( "English TWL", "TWL", new Dictionary_TWL() );
					break;
				}
				case "ZINGA":
				{
					result = new Dictionary( "Italiano ZINGA", "ZINGA", new Dictionary_ZINGA() );
					break;
				}
			}
			*/
			_currDictionary = result;
			
			return result;
		}
	}
}