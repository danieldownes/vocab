package util
{
	
	public class Iterator 
	{
		private var _list:ArrayList;
		private var _listSize:int;
		private var _accessCounter:int;
		
		public function Iterator(list:ArrayList) 
		{
			_list = list;
			_listSize = _list.size();
			_accessCounter = 0;
		}
			
		public function hasNext():Boolean
		{
			return _accessCounter < _listSize;
		}
		
		public function next():Object
		{
			var val:Object = _list.get(_accessCounter);
			
			_accessCounter++;
			
			return val;
		}
	}

}