package util 
{
	public class Random 
	{
		public function Random() 
		{		
		}
		
		/** 
		* Generates a truly "random" number
		* @return Random Number
		*/ 
		public function randomNumber(low:Number=0, high:Number=1):Number
		{
			return Math.floor(Math.random() * (1+high-low)) + low;
		}
		
		public function nextIntRange(low:int = 0, high:int = 1):int
		{
			return int(randomNumber( low, high ));
		}
	}
}