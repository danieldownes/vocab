﻿package
{
	public class ArrayList 
	{
		private var elements:Array;
		
		public function ArrayList() 
		{
			elements = new Array();
		}
		
		public function add(item:Object):void 
		{
            elements.push(item);
        }
		
		public function clear():void 
		{
            elements.splice(0, (elements.length - 1));
        }
		
		public function get(index:int):Object 
		{
			return elements[index];	
		}
		
		public function remove(obj:Object):void
		{
			var i:int = 0;
			
			for (i = 0; i < elements.length; i++)
			{
				if (obj == elements[i])
				{
					elements.splice(i, 1);
					break;
				}
			}
		}
		
		public function size():int
		{
			return elements.length;
		}
		
		public function iterator():Iterator
		{
			return new Iterator(this);
		}
		
	}

}