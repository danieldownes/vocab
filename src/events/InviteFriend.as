﻿package events
{
	import flash.events.Event;	
	
	public class InviteFriend extends Event
	{
		public static const INVITE_FRIEND:String = "inviteFriend";
		public var id:uint;
		
		public function InviteFriend(id:uint, bubbles:Boolean)
		{
			super(INVITE_FRIEND,bubbles);
			this.id = id;			
		}		
	}	
}