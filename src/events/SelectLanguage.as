﻿package events {
	
	import flash.events.Event;	
	
	public class SelectLanguage extends Event 
	{
		public static const SELECT_LANGUAGE:String = "selectLanguage"
		public var _languageId:uint;
		
		public function SelectLanguage(languageId:uint, bubbles:Boolean = false) 
		{
			super(SELECT_LANGUAGE, bubbles);
			this._languageId = languageId;
		}
	}
}
