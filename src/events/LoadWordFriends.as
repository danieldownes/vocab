﻿package events  {
	import flash.events.Event;	
	public class LoadWordFriends extends flash.events.Event {
		public static const LOAD_WORD_FRIENDS:String = "loadwordfriends"
		public function LoadWordFriends() {
			super(LOAD_WORD_FRIENDS);
		}		
	}	
}