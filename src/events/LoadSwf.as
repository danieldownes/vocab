package events
{
	import flash.events.Event;	
	
	public class LoadSwf extends Event 
	{
		public static const LOAD_SWF:String = "loadSwf";
		public var ind:int;
		
		public function LoadSwf(_ind:uint, bubbles:Boolean)
		{
			super(LOAD_SWF,bubbles);			
			ind = _ind;
		}		
	}	
}