﻿package events
{
	import flash.events.Event;	
	
	public class RemoveRequest extends Event
	{
		public static const REMOVE_REQUEST:String = "removeRequest";
		public var requestId:String;
		public var requestType:String;
		
		public function RemoveRequest(requestId:String, requestType:String, bubbles:Boolean)
		{
			super(REMOVE_REQUEST,bubbles);
			this.requestId = requestId;
			this.requestType = requestType;
		}		
	}	
}