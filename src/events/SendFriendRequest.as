﻿package events  {
	import flash.events.Event;	
	public class SendFriendRequest extends Event {
		public static const SEND_FRIEND_REQUEST:String = "SendFriendRequest"
		public var frdArr:Array;
		
		public function SendFriendRequest(arr:Array,bubbles:Boolean) {
			super(SEND_FRIEND_REQUEST,bubbles);
			frdArr = arr;			
		}		
	}	
}