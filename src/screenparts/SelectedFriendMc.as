﻿package screenparts
{
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	
	
	public class SelectedFriendMc extends SelectedFriendMc_design
	{
		
		public var id:uint;
		public var ind:uint;
		public var nameStr:String;
		
		private var _imageLoader:Loader;
		
		public function SelectedFriendMc() {
			// constructor code
			_imageLoader = new Loader();
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		public function setup(id:uint,ind:uint,nameStr:String):void{
			_imageLoader.load(new URLRequest("http://graph.facebook.com/" + id + "/picture?type=square"));
			this.id = id;
			this.ind = ind;
			this.nameStr = nameStr
		}
		private function onImageLoadingComplete(evt:Event):void{
			_imageLoader.width = 45;
			_imageLoader.height = 39;
			imageMc.removeChildAt(0);
			imageMc.addChild(_imageLoader);
			_imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		private function onIOError(evt:IOErrorEvent):void{
			_imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
	}
}
