﻿package screenparts
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.errors.IOError;
	import flash.net.URLRequest;
	import flash.events.MouseEvent;
	
	import game.LanguageManager;
	
	
	public class RequestListMc extends RequestListMc_design
	{
		public var id:String;
		public var reqId:String;
		public var fullName:String;
		public var from_score:int;
		public var to_score:int;
		public var gameMode:int;
		public var permutation:String;
		
		private var _gameModeName:String;
		private var _imageLoader:Loader;
		
		
		public function RequestListMc()
		{
			_imageLoader = new Loader();
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		
		public function setup(id:String, reqId:String, fullName:String, score:int, gameMode:String, permutation:String):void
		{
			this.gotoAndStop( LanguageManager.getInstance().getLanguage() + 1);
			
			trace("Request screen setup reqId:" + reqId);
			
			this.reqId = reqId;
			this.fullName = fullName;
			this.from_score = score;
			this.gameMode = Number(gameMode);
			this.permutation = permutation;
			
			_imageLoader.load(new URLRequest("http://graph.facebook.com/" + id + "/picture?type=square"));
			
			if( fullName != null)
				nameTxt.text = fullName;
			
			var sScored:String = LanguageManager.getInstance().getTranslation("SCORED");
			
			setGameModeName(gameMode);
			this.nameTxt.text = sScored + ": " + from_score + " (" + _gameModeName + ")";
			
			this.id = id;
		}
		
		
		public function setupResult(id:String, fullName:String, $to_profileId:int, to_score:int, from_score:int, gameMode:String, $challengeId:String):void
		{
			// $fId=$name=$to_profileId=$to_score=$from_score=$gameMode
			this.gotoAndStop( LanguageManager.getInstance().getLanguage() + 50);
			
			var sScore:String;
			var sResult:String;
			
			this.reqId = $challengeId;
			this.fullName = fullName;
			this.from_score = from_score;
			this.to_score = to_score;
			this.gameMode = Number(gameMode);
			this.permutation = permutation;
			
			setGameModeName(gameMode);
			
			
			_imageLoader.load(new URLRequest("http://graph.facebook.com/" + id + "/picture?type=square"));
			
			if( from_score > to_score)
			{
				sResult = LanguageManager.getInstance().getTranslation("WON_TO");
				sScore = from_score + ":" + to_score;
			}else if( from_score < to_score)
			{
				sResult = LanguageManager.getInstance().getTranslation("LOST_TO");
				sScore = to_score + ":" + from_score;
			}else
			{
				sResult = LanguageManager.getInstance().getTranslation("TIED_TO");
				sScore = to_score + ":" + from_score;
			}
			
			if( fullName != null)
				sResult += " " + fullName;
			
			nameTxt.text = sResult;
			
			sScore += " (" + _gameModeName  + ")";
			
			labelTxt.text = sScore;
			
			this.id = id;
		}
		
		private function setGameModeName(gameMode:String):void
		{
			var iGm:uint = Number(gameMode);
				
			if( iGm == 10)
				_gameModeName = LanguageManager.getInstance().getTranslation("CLASSIC_MODE");
			else if( iGm == 12)
				_gameModeName = LanguageManager.getInstance().getTranslation("SPEED_MODE");
			else if( iGm == 13)
				_gameModeName = LanguageManager.getInstance().getTranslation("GRID_MODE");
		}
		
		private function onImageLoadingComplete(evt:Event):void
		{
			_imageLoader.width = 40;
			_imageLoader.height = 40;
			imageMc.addChild(_imageLoader);
			_imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		private function onIOError(evt:IOErrorEvent):void
		{
			_imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
	}
}
