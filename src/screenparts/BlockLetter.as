﻿package screenparts
{
	public class BlockLetter extends BlockLetter_design {
		public var finded:Boolean = false;
		public function BlockLetter() {
			
		}
		public function setup(str:String):void{
			letterTxt.text  = str;
			letterTxt.visible = false;
		}
		public function showLetter():void{
			finded = true;
			letterTxt.visible = true;
		}
	}
	
}
