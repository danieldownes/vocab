﻿package screenparts
{
	import caurina.transitions.Tweener;
	
	
	public class LetterMc extends LetterMc_design
	{
		private const ANIMATION_TIME:Number = 0.75;
		
		public var addedToList:Boolean = false;
		public var animation:Boolean = false;
		public var ind:uint;
		public var placedInd:uint;
		
		public function LetterMc() {
		}
		
		public function setup(txt:String):void
		{
			letterTxt.text = txt;
		}
		
		public function moveToPos(valX:Number,valY:Number):void{
			Tweener.addTween(this,{x:valX, y:valY,time:ANIMATION_TIME,onComplete:finish})
		}
		
		public function checkTweening():Boolean{
			if(Tweener.isTweening(this)){
				return true
			}
			return false;
		}
		
		public function removeTweening():void{
			Tweener.removeTweens(this);
			animation = false;
		}
		
		private function finish():void{
			animation = false;
		}
	}
	
}
