﻿package screenparts
{
	
	import flash.display.MovieClip;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.errors.IOError;
	import flash.events.MouseEvent;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	import game.LanguageManager;
	
	public class FriendListMc extends FriendListMc_design
	{
		public var id:String;
		private var _imageLoader:Loader;
		private var urlLoader:URLLoader;
		private var urlRequest:URLRequest;
		
		
		public function FriendListMc()
		{
			_imageLoader = new Loader();
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		
		public function setup(id:String, fName:String, wins:uint, losses:uint, str:String, bMe:Boolean = false):void
		{
			// Get words for current language
			var winsTEXT:String = LanguageManager.getInstance().getTranslation("WINS");
			var winTEXT:String = LanguageManager.getInstance().getTranslation("WIN");
			var lossesTEXT:String = LanguageManager.getInstance().getTranslation("LOSSES");
			var lossTEXT:String = LanguageManager.getInstance().getTranslation("LOSS");
			
			if( bMe)
				this.gotoAndStop(2);
			else
				this.gotoAndStop(1);
			
			try
			{
				urlRequest = new URLRequest("facebookProxy.php");
				var variables:URLVariables = new URLVariables();
				variables.path = "http://graph.facebook.com/" + id + "/picture?type=square";
				urlRequest.data = variables;
				
				urlLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
				urlLoader.addEventListener(Event.COMPLETE, completeHandler);
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				urlLoader.load(urlRequest);
			}catch(e:Error)
			{
			}
			
			trace("name:" + fName);
			
			if (fName != null)
				nameTxt.text = fName;
			
			// Construct score/results text
			if( wins != 1)
				scoreTxt.text = winsTEXT + " " + wins + " ";
			else
				scoreTxt.text = winsTEXT + " 1 ";
			if( losses != 1)
				scoreTxt.appendText(lossesTEXT + " " + losses);
			else
				scoreTxt.appendText(lossTEXT + " 1 ");
				
			//if (str != null)	// TODO: What was this?
			//	labelTxt.text = str;
			
			trace("str:" + str);
			this.id = id;
		}
		
		
		private function completeHandler(event:Event):void
		{
			try
			{
				var byteArray:ByteArray = urlLoader.data;

				urlLoader.removeEventListener(Event.COMPLETE, completeHandler);
				
				_imageLoader = new Loader();
				_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadingComplete);
				_imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				_imageLoader.loadBytes(byteArray);
			}catch(e:Error)
			{
			}
		}
		
		
		private function onImageLoadingComplete(evt:Event):void
		{
			_imageLoader.width = 40;
			_imageLoader.height = 40;
			imageMc.addChild(_imageLoader);
			_imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		
		private function onIOError(evt:IOErrorEvent):void
		{
			_imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadingComplete);
			_imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
	}

}
